#### Designer Pattern Composite
- Permite que você crie hierarquia de objetos
- Permite que você trate objetos individuais e objetos agrupados da mesma forma (homogênea)

#### Component
- É uma super classe, pode ser uma interface ou uma classe abstrata. Que possui as possue as operações possível em determinado objeto

#### Leaf
- Ele representa o final da árvore, como se fosse uma folha, já que esta classe é como se fosse final, não possui filhos. 

#### Composite
- é um agrupador de componentes
- normalmente ele possui uma lista de outros components
```java 
public class Composite extends Component {

    private List<Component> children = new ArrayList<Component>();

    public void add(Component component) {
        children.push(component)
    }

    public void remove(Component component) {
        children.remove(component);
    }

    @Override
    public void action() {
        for(Component c : children) {
            c.action();
        }
    }
}
````
