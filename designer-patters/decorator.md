### Decorator ou Wrapper
- Tem a função de adicionar comportamentos ou estados a Objetos Existentes
- Encapsula um objeto adicionando uma nova funcionalidade


#### Component
- É uma classe abstract ou uma interface

#### Concret Component
- Implementa ou extende do Component

#### Decorator
- É uma classe abstract que extende ou implementa o Component
- Ele possui um component dentro dele que é passado no constructor (Classes abstractas podem ter constructores públicos, já que quem extender esses classes, deverá chamar o super no constructor)

Decorators devem extender de Component para que possam ser combinados vários decorates, aí que está o pulo do gato. Não acabamos de definir que o decorator recebe um component no constuctor? E se o component que ele recebe fosse outro decorator? Pronto, agora podemos combinar.
