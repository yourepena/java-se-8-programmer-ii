é muito parecido com um method factory, a diferença é que a classe é abstrata e possui um método que cria um determinado objeto. 


Normalmente Nosso Cliente irá utilizar um Producer de Factory, que é uma classe que normalmente possui um método estático onde retorna uma factory específica. 

Para isto funcionar, precisamos criar uma abstract factory que seja pai de outras factories que irão cuidar de fato da criação dos seus produtos

```java
class abstract ProdutoAbstractFactory {
    public abstract Produto newProduto(String tipo);
}

class ProdutoOrganicoFactory extends ProdutoAbstractFactory {
    public Produto newProduto(String tipo) {
        if( tipo.equals("tomate")) {
            return PepinoOrganico();
        }else if( tipo.equals("cenoura")) {

        }
    }
}
```

