### Singlenton

É um dos padrões de projeto mais utilizados, ele basicamente mantém uma única instancia de uma classe para toda a aplicação.
- Possui um constructor privado
- E os clientes conseguem pegar essa instância através do método `getInstance()`

***Necessidade***
- Criar um gerenciador de segurança para aplicação toda.
- Ele não tem sentido de ser instanciado mais de uma vez.

***Problema***
- Não tenho como garantir que o gerenciador de segurança seja instanciado somente uma vez

**Dica**:
Podemos instancer diretamente a variavel instance na declaração, garantindo que sempre esta esteja notNull.
Esta abordagem não é recomendada quando o singleton consome muitos recursos, já que a JVM irá instanciar o objeto, mesmo sem você precisar dele.

- O seguinte exemplo é um Singleton que se protege de concorrência de Threads.

```java
public class SecurityManager {

    private static SecurityManager instance;

    private SecurityManager() {}

    public static SecurityManager getInstance() {
        if(this.instance === null){
            synchronized (SecurityManager.class) { // Poderiamos adicionar a propriedade synchronized ao método como um todo, mas estariamos synchrinizando coisas demais.
                if (this.instance === null) { // Validamos mais uma vez dentro do bloco synchronized se o instance está null.
                    this.instance = new SecurityManager();
                }
            }
        }
        return this.instance;
    }
}

```
