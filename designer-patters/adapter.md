
### Designer Pattern adpater
- Pertence ao grupo Structural Pattern
- O adaptador faz a ponte para os componentes se comunicarem.
- O Designer Pattern adpater possui duas formas de trabalhar, Object Adapter e Class Adapter

#### Class Adapter
- Extende da classe Target
- Implementa os mesmos métodos que a super classe, só que nesse caso ele adapta o comportamento, mas chama o método do pai sempre.
```java
public class TemperatureClassAdapter extends Temperature {
	@Override
	public void setValue(double value) {
		super.setValue(value * 9 / 5 + 32);
	}
	@Override
	public double getValue() {
		return (super.getValue() - 32) * 5 / 9;
	}
	public double getValueInFahrenheit() {
	
		return super.getValue();
	}
}
```
#### Object Adapter
- Possui um atributo do tipo Target, ou seja é uma composição 
- Esse atributo sempre é passado no constructor
- Possui métodos iguais ao Target e invoca os métodos dele.
```java
public class TemperatureObjectAdapter {
	private Temperature temperature;
	public TemperatureObjectAdapter(Temperature temperature) {
		this.temperature = temperature;
	}
	public double getValue() {
		return (temperature.getValue() - 32) * 5 / 9;
	}
	public void setValue(double value) {
		temperature.setValue(value * 9 / 5 + 32);
	}
}
```
