### O que é um pattern?
- Cada padrão descreve um problema que ocorre muitas vezes em nosso ambiente, e então descreve a solução central para este problema, de forma que você pode utilizar esta solução 1milhão de vezes, sem sequer utilizá-la da mesma forma. 

### Definindo
Um disgner pattern precisa das seguintes componentes.
- Nome, Problema, Solução, Consequência

### Gof Desginer Patterns
- Propões 23 designer patters
- São dividios em 3 categorias
    - Creational Patterns
        - ***Factory Method***
        - ***Abstract Factory***
        - Builder
        - Prototype
        - ***Singleton***
    - Behavioral Patterns (Como os objetos ou classes se comunicam)
        - Interpreter
        - ***Template Method*** 
        - Chain of Responsability
        - Command
        - Iterator
        - Mediator
        - Memento
        - ***Observer***
        - State
        - ***Strategy***
        - Visitor
    - Structural Patterns
        - ***Adapter***
        - Bridge
        - ***Composite***
        - ***Decorator***
        - Facade
        - Flyweigth
        - Proxy

