### Observer
- Funciona no pradão de projeto onde você tem observadores e observados
- Toda vez que um observado tiver uma mudança todos os observadores deverão ser notificados

#### Subject
- Possui o método para notificar os observers
- Possui ainda métodos para adicionar ou remover os observer para poder notifica-los
```java
public interface Subject {
	public void registerObserver(Observer observer);
	public void unregisterObserver(Observer observer);
	public void notifyObservers();
}
```

### Concrete Subject
- É quem possui alguma informação a ser observada
- Normalmente é um singleton
- Implementa os métodos do Subject
- Possui um Set de Observer que irá ser notificado
- Devemos registrar o Observer para poder notificar ele

### Observer
- Possui um método que será chamado pelo subject.
```java
public interface Observer {
	public void update(Subject subject);
}
```

### ConcreteObserver
- Implementa o método que será notificado


