### Stream API
- Uma API que permite combinar operações sobre coleções;
- Aproveita as facilidade da utilização do lambda;
- Funciona muito bem para Collections pequenas e grandes;
    - Usa a abordagem lazy evaluation;
- Permite realizar encadeamento de operações chamado de pipeline;
```java
List<Integer> newList = list.stream().sort().filter(e -> e > 2 && e < 8).map(e-> e*e).collect(Collectors.toList());
```

### Obtendo um objeto do tipo Stream
- As List e as Set possuem um método chamado stream;
- No caso para array, podemos usar o método estático da própria classe Stream ```Stream.of(array);```

### Operações da Stream API
- Podemos dividir em dois tipos
    - Intermediárias
        - Retornam um novo tipo de Stream
        - Possibilitam o Pipeline
    - Terminais
        - Geram um resultado final (redução)
        - Finalizam o uso da stream
        - Ex: collect(), reduce(), count(), max()
 
### Operação: sorted()
- Ordena os elementos da coleção
- Permite fornecer ou não um Comparator<T>
- Operação intermediária ```stream.sorted((e1, e2) -> e1.getNome().compareTo(e2.getNome()))```

### Operação: limit()
- Define um tamanho máximo para a coleção
- Os elementos que excedem o limite fornecido são removidos
- Operação intermediária ```stream.limit(10);```

### Operação: filter()
- Filtra os resultados de acordo com um critério
- Recebe um parâmetro Predicate<T>
- Operação intermediária ```stream.filter(e -> e > 10);```

### Operação: distinct()
- Remove elementos duplicados
- Operação intermediária ```stream.distinct();```

### Operação: map()
- Mapeia um elemento em outro elemento (transformação)
- Recebe um parâmetro Function<T, R>
- Operação intermediária stream.map(e -> e + 2); 
- Existem também mapeamentos especializados
    - mapToInt() −> IntStream
    - mapToDouble() −> DoubleStream
    - mapToLong −> LongStream

### Operação: collect()
- Finaliza o pipeline, gerando um resultado
- Operação terminal
- A classe Collectors tem métodos estáticos que auxiliam nesta operação
- Exemplos:
```java
stream.collect(Collectors.toList()) => List<T>
stream.collect(Collectors.toSet()) => Set<T>
stream.collect(Collectors.counting()) => Long
stream.collect(Collectors.summingInt(e -> e.getIdade())) => Integer
```

### Operação: count()
- Faz a contagem de elementos
- Operação terminal ```long c = stream.count();```

### Streams Paralelas
- Uma stream paralela pode ser processada simultaneamente por vários núcleos de processamento
- Para que o resultado seja consistente, as operações intermediárias precisam ser independentes
```java
List<Integer> list = list.parallelStream()
.sorted()
.map(e -> e * e)
.collect(Collectors.toList());
 ```

 
