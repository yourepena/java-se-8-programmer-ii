### O que é uma InnerClass
- Inner classes são classes declaradas dentro de outras classes
- Quando compiladas, geram bytecodes diferentes
    - MyOuter.class
    - MyOuter$MyInner.class

#### As inner classes podem ser divididas em 4 tipos
- Regular inner class
- Method-local inner class
- Anonymous inner class
- Static inner class

#### Regular inner class
- Declarada como membro de uma classe
- Possui acesso aos elementos da classe dentro da qual está inserida

#### Instanciando uma inner class
- Uma instância de uma inner class não pode existir sem estar associada a uma instância de uma outer class
- Normalmente é a outer class que instancia a inner class

#### Obtendo a referência de uma inner class 
- O operador this referencia o próprio objeto
- Dentro de uma inner class, this referencia a instância da inner class
- E para referenciar a outer class?
```java
public class MyOuter {
    private class MyInner {
        public void m() {
            System.out.println("Ref inner: " + this);
            System.out.println("Ref outer: " + MyOuter.this);
        }
    }
}
```

### Instanciando uma inner class 
```java
public class OuterClass {
	
	private String greeting = "Hi";
	public class InnerClass {
		public int repeat = 3;
		public void go() {
			for (int i = 0; i < repeat; i++) {
				System.out.println(greeting);
			}
		}
	}
	
	public void callInnerClass() {
		InnerClass ic = new InnerClass();
		ic.go();
	}
	
	public static void main(String[] args) {
		OuterClass oc = new OuterClass();
		InnerClass ic = oc.new InnerClass(); // Esquisito
	}

}

```

#### Method-local inner class
- Declarada dentro de um método
- Apenas o método enxerga a classe
- A inner class pode acessar variáveis locais do método, desde que estas sejam final ou não tenham seu valor alterado
```java
public class MyOuter {
    public void m() {
        final String msg = "Mensagem!";
        class MyInner {
            public void imprimirMensagem() {
                System.out.println(m);
            }
        }
        MyInner i = new MyInner();
        i.imprimirMensagem();
    }
}
```

#### Anonymous inner class
- Não possui nome
- Classes anônimas são sempre subclasses de uma classe ou implementação de uma interface
- Sobrescrevem ou implementam métodos da superclasse ou interface
```java
public class Porta {
    public void abrir() {
        System.out.println("abrir");
    }
}
public class Casa {
    private Porta p = new Porta() {
        public void abrir() {
            System.out.println("porta anônima");
        }
    };
    public void m() {
        p.abrir();
    }
}
```

#### Static inner class
- Não é realmente uma inner class porque não tem um relacionamento especial com a outer class
- Ela é apenas uma classe declarada dentro de outra classe
- Basta declarar a classe como static
- Uma static inner class famosa no Java é a Map.Entry, cujos objetos são retornados quando o método entrySet() é invocado
```java
public class MyOuter {
    static class MyInner {
        public void imprimir() {
            System.out.println("Mensagem!");
        }
    }
}
MyOuter.MyInner i = new MyOuter.MyInner();
i.imprimir();
```

### Summary


| Properties | Member inner class | Local inner class | Anonymous inner class | static nested class  |
|:-------------| :-------------| :-------------| :-------------| :-------------|
| **Acces Modifiers alloweds** | Public, protected, private or default| None, Already local to method | None. Already local to statemant  | Public, protected, private or default |
| **Can extends any class and any number of interface?**  | Yes | Yes | No, must have exactly one superclass or one interface | yes |
| **Can be abstract?** | Yes | Yes | N/A | Yes |
| **Can be final?** | Yes | Yes | N/A | Yes |
| **Can access instance members of enclosing class?** | Yes | Yes | Yes | No (not directly) |
| **Can access local variables of enclosing class?** | No | Yes - If final or effectively final | Yes - If final or effectively final | No |
| **Can declary static methods?** | No |No |No | Yes |




