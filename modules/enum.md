- Trabalhar com enum é bom porque garante o type-safe checking. 
- Mesmo sendo constantes públicas o seguinte código `NÃO` é permitido.
```java
enum Nomes {
	public static final YOURE,
}
```
- Quando printamos um enum comum, é chamado o toString() da classe, por tanto às vezes só recebemos por exemplo o número do índice da constate, mas nunca podemos comparar um enum com um int
```java
if ( Season.SUMMER == 1) {} // Não compila
```
- Podemos criar também um enum a partir de uma string
```java
Season s1 = Season.valueOf("SUMMER"); // ok 
Season s2 = Season.valueOf("summer"); // IlegallArgumentoException
```
- Outra coisa que não podemos fazer é extender de um enum
- Podemos adicionar um constructor no enum, claro agora temos que declaras esse construtor nas nossas constantes. 

```java 
enum Season {
    SUMMER("high"), WINTER("low"), SPRING("medium"), FALL("medium"); // Semicolon no final da linha
    private String publico; // Declaração da variável, é importante ressaltar que a variável pode ser pública, exemplo de utilização caso for pública Season.SUMMER.publico
    private Season(String publico) { // Constructor privado.
        this.publico = publico;
    } 

}
```
- Podemos declarar também métodos dentro de um enum da seguinte forma:
```java
enum Season {
    SUMMER("high") {
    	public void print() { // Este método somente poderá ser chamado se ele for a sobrescrita de um método do enum.
        	System.out.println("Mais horas");
        }	
    }, WINTER("low"), SPRING("medium"), FALL("medium");// Semicolon no final da linha
	public String publico;
    private Season(String publico) { // Constructor privado.
        this.publico = publico;
    } 
    public void print() {
    	System.out.println("Default horas");
    }
}
```

