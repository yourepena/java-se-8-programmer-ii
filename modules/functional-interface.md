### Functional Interface
- São interfaces com um único método abstrato declarado
- São utilizadas para as expressões lambdas (Predicate)


Ex:
```java
@FunctionalInterface
interface EstaComFOme {
	boolean teste(String a);
}

public class Leao {
	
	public boolean fome;
	
	public Leao(String value, EstaComFOme fome) {
		this.fome = fome.teste(value);
	}
	
	public static void main(String[] args) {
		Leao l = new Leao("asdasd", a ->  a.isEmpty());
		System.out.println(l.fome ? "Está com fome" : "Não está com fome");
	}
	
}
```

