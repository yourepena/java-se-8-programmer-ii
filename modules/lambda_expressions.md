#### O que são expressões lambda 
- Maior inovação da versão 8 do Java
- O nome lambda vem do conceito matemático de cálculo lambda
- Expressões lambda trazem o Java mais próximo do paradigma de programação funcional
- Uma expressão lambda pode ser considerada como um objeto
    - Pode ser referenciada por uma variável 
    - Pode ser passada como parâmetro para métodos e utilizada como retorno
- Em Java, uma expressão lambda é utilizada em substituição a uma inner class anônima
- Uma expressão lambda é representada da seguinte forma ```(Parâmetros) -> { Corpo }```
```java
Runnable r = new Runnable() {
    @Override
    public void run() {
     System.out.println("ABC");
    }
};
new Thread(r).start();

Runnable r = () -> System.out.println("ABC");
new Thread(r).start();

new Thread(() -> System.out.println("ABC")).start();
```
#### Interfaces funcionais
- Uma interface funcional tem duas características
    1. É uma interface
    2. Possui apenas 1 método
- Uma expressão lambda pode ser atribuída a uma variável de uma interface funcional

#### @FunctionalInterface
- Esta annotation define uma interface como interface funcional
```java
@FunctionalInterface
public interface Generator {
    public String generate();
}
```
• Seu uso não é obrigatório
• Se @FunctionalInterface for utilizada, o
compilador checa se a interface define
apenas 1 método