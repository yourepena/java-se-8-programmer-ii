### Generics 
 - Foi introduzido a partir do Java 5.
 - Permitem a parametrização de tipos de dados.

Utilizamos generics quando queremos trabalhar com objetos genéricos mas queremos garantir o tipo em todos meus componentes. Além de oferecer mais controle evita utilizar cast explícitos.

O seguinte código compila. Atribuo um array de subclasse para a classe pai.
```java
Cachorro[] cachorros = new Cachorro[2];
cachorro[0] = new Cachorro();
cachorro[1] = new Cachorro();
Animal[] animais = cachorros;
```

Mas o seguinte código não compila. Em generics a atribuição só pode ser feita se o tipo parametrizado é o mesmo, não importa harança.
```java
List<Cachorro> cachorros = new ArrayList<Cachorro>();
cachorros.add(new Cachorro());
cachorros.add(new Cachorro());
List<Animal> animais = cachorros;
```

### Wildcard:
 - Utilizado para deixar em aberto o tipo parametrizado.
 - É representado por "?"
 - Deixa flexível o tipo de objeto passado no generics.
 - Internamente trabalhamos sempre com object porque não sabemos que tipo de parametro está sendo passado.

```java
public void imprimir(Collection<?> c){
    for(Object o : c){
        System.out.println(o)
    }
}
```

### Wildcard Extends
    - Quando é necesspario deixar o tipo de dado em aberto mas garantir que este tipo deve estender de determinada classe, é possível usar o extends.
```java
public void imprimir(Collection<? extends Animal> c){
    for(Object o : c){
        System.out.println(o)
    }
}
``` 
 - Quando a restrição deve ser feita a classe que implementam uma interface, o extends também é usado.
 - Quando utilizamos extends é usado não é possível adicionar elementos

### Wildcards Super
    - Quando é necessário deixar o tipo do dado em aberto mas queremos garantir que este tipo deva ter uma determinada superclasse, é possível usar o super.
```java
public void imprimir(Collection<? super Cachorro> c){
    for(Object o : c){
        System.out.println(o)
    }
}
``` 
    - A coleção pode ser de qualquer tipo que seja uma superclasse de Cachorro ou a própria classe Cachorro.
    - Quando o super é usado, a coleção pode receber novos elementos.

### Definindo métodos com generics
    - Além do tipo parametrizado pode ser definido a nível de classe, ele também pode ser definido a nível de método.
```java
public <T> List<T> criarLista(T e){
    List<T> l = new ArrayList<T>();
    l.add(e);
    return l;
}
```

