1. [x] B
2. [x] D
3. [x] E
4. [x] E
5. [x] B, C, F
6. [ ] F - `C` When we don't use generics, we give compiler erros, however we can call methods that are defined on the Object
7. [X] A, D
8. [X] C
9. [X] E
10. [ ] E - `A` The array is sorted using `MyComparator`, wich sorts the elements in reverse alphabetical order in a case-insensitive fashion. Normally, numbers sort before letters. THis code reverses that by calling the `compareTo()` method on b instead of a
11. [X] A
12. [ ] A, B - `D` The follow method is correct: `Helper.<NullPointerException>printException(new NullPointerException("C"))`
13. [X] B, E 
14. [ ] D - `C` - I forget that a class Sorted implemented a Compartor interface
15. [X] D
16. [ ]  B, D, F - The java.lang.Comparable
17. [x] B, D - 
18. [ ] A, B, C - All of the choices that mention class C are incorrect because it no longer means the class C.
19. [x] A, D - Queue has only the remove by object method
20. [x] E - 
21. [x] A, F
22. [x] B
23. [X] B, E
24. [ ] F - The syntax for a lambda is correct, however, s is already defined as a local variable and therefore the lambda can't redefine it.
25. [ ] D - The second call to `merge()` sees that the map currently has a null value for that key. It does not call the mapping function but instead replaces it with the new value of 3.
