# Generics and Collections
- Generics and Collections
    - [x] Create and use generic class
    - [x] Create and use ArrayList, TreeSet, TreeMap and ArrayDeque objects
    - [x] Use java.util.Comparator and java.lang.Comparable interfaces
    - [x] Iterate using forEach methods on Streams and List
    - [X] Use method references with Streams
- Advanced Java Class Design
    - [x] Create and use lambda expressions
- Generics and Collections 
    - [x] Filter a collection using lambda expressions
- Java Stream API
    - [x] Use of `merge()` and `flatMap()` methods of the Stream API

## Reviewing OCA Collections
In the following sections, we will review arrays, ArrayLists, wrapper classes, autoboxing, the diamond operator, searching and sorting.

### Array and ArrayList
- An ArrayList is an object that contains other objects.
- An ArrayList cannot contain primitives
- An array is a built-in data structure that contains other objects or primitivies

```java
import java.util.*;
class Playground {
    public static void main(String[ ] args) {
       String array[] = {"a", "b"};
       List<String> list = Arrays.asList(array);
       list.set(1, "test");
       array[0] = "new";
       String[] array2 = (String[]) list.toArray();
       list.remove(1); // throws UnsupportedOperationException
    }
}
```
- Line 9 shows that list is not resizable because it is backed by the underlying array

### Searching and Sorting
```java
int[] numbers = {6, 9, 1, 8};
Arrays.sort(numbers);
System.out.println(Arrays.binarySearch(numbers, 6));
System.out.println(Arrays.binarySearch(numbers, 3));
// List 
List<Integer> numbers = Arrays.asList(6, 9, 1, 8);
Collections.sort(numbers);
System.out.println(Collections.binarySearch(numbers, 3)); // 0
System.out.println(Collections.binarySearch(numbers, 2)); // -1
```
### Wrapper Classes And Autoboxing

| Primitive type | Wrapper class | Example of initializing | Size
| ------ | ------ | ------ | ------ |
| boolean |  Boolean | new Boolean(true) | 1 bit |
| byte | Byte | new Byte((byte) 1) | 8 bits |
| short | Short | new Short((short) 1) | 16 bits | 
| int |  Integer | new Integer(1) | 32 bits |
| long |  Long | new Long(1) | 64 bits |
| float |  Float | new Float(1.0) | 32 bits |
| double |  Double | new Double(1.0) | 64 bits |
| char |  Character | new Character('c') | 16 bits |

``` java
List<Integer> numbers = new ArrayList<Integer>();
numbers.add(1);
numbers.add(new Integer(3));
numbers.add(new Integer(5));
numbers.remove(1);
numbers.remove(new Integer(5));
System.out.println(numbers);
```

## Working with Generics
- Generics fix a specific class by allowing you to write and use parameterized types.

### Generic Classes
```java
public class Create<T> {
    private T contents;
    public T emptyCreate() {
        return contents;
    }
    public void packCreate(T contents) {
        this.contents = contents;
    }
}
```
- The generics type is in everywhere
- When you instantiate the classe, you tell the compiler what T should be for that particular instance
- Numeric literals have been able to contain underscores since Java 7
- Behind the scenes the compiler replaces all references to T with Object.
- This means there is only one class file
- This process of removing the generics syntax from your code is referred to as `type erasure`

### Generics Interfaces
- Just like a class, an interface can declare a formal type parameter.
- There are three ways a class can approach implementing a interface with generic
    - The first is to specify the generic type in the class
    - The next way is to create a generic class
    - The final way is to not use generics at all. Its generates a compiler warning, but it does compile

### Generic Methods
In this example, the method uses a generic parameter:
```java
public static <T> Create<T> ship(T t) {
    System.out.println("Preparing " + t);
    return new Create<T>();
}
```
- Generics methods are often useful for static methids since they aren't part of an instance that can declare the type.

This example does not compile beacuse it's omits the formal parameter type
```java
public static T noGood(T t) { return t;}
```
### Interacting with Legacy Code
This code compile fine, because the method is a legacy code and reciev a reference of generic List, but the code throws a ClassCastException in Runtime due to a List haves one Dragon in the zero position
```java
class Playground {
    public static void main(String[ ] args) {
       java.util.List<Unicorn> unicorns = new java.util.ArrayList<>();
       addUnicorns(unicorns);
       Unicorn uni = unicorns.get(0); // ClassCastException
    }
    public static void addUnicorns(java.util.List dragons) {
        dragons.add(new Dragon());
    }
}
class Unicorn {}
class Dragon {}
```
- Autoboxing has a different problem, It's fails with a compiler error rather than a runtime error.

### Bound
- A **bounded parameter type** is a generic type that specifies a bound for the generic.
- A **wildcard generic type** is an unknown generic type represented with a question mark (?).

You can use generic wildcards in three ways. 

| Type of bound | Syntax | Example | 
| ------ | ------ | ------ |
| Unbounded wildcard |  ? | `List<?> l = new ArrayList<String>();` |
| Wilcard with an upper bound | ? extends type | `List<? extends Exception> l = new ArrayList<RuntimeException>();` |
| Wilcard with a lower bound | ? super tpye |  `List<? super Exception> l = new ArrayList<Object>();` |

```java
List<Object> l = new ArrayList<String>(); // it does NOT COMPILE
Interger[] numbers = { new Integer(32) };
Object[] objects = numbers;
objects[0] = "forty two"; // throws ArrayStoreException
```

### Unbounded wilcards
- An `unbounded wilcard` represents any data type. 
- You use `?` when you want to specify that any type is OK with you]

`printList()` takes any type of list as a parameter, so the code does compile well.
```java
public static void printList(List<?> list) {
    for(Object obj : list) { System.out.println(obj); }
}
public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    list.add("something");
    printList(list);
}
```

### Upper-Bounded Wildcards
The problem stems from the fact that Java doesn't know what type `List< ? extends Bird>` really is.
```java
class Playground {
    public static void main(String[ ] args) {
       java.util.List<? extends Dragon> dragons = new java.util.ArrayList<Dragon>();
       dragons.add(new Unicorn()); //DOES NOT COMPILE, because we can't add a Sparrow to Lis<Bird>
       dragons.add(new Dragon()); //DOES NOT COMPILE, because we can't add a Bird to Lis<Sparrow>
    }
}
class Unicorn extends Dragon {}
class Dragon {}
```
- For interfaces we use the same keyword `extends`
- A diference beetwen a paramater `List<SuperClass>` and `List<? extends SuperClass>`:
    - A List<Superclass> can be passed to either methods
    - A variable of type List<Child> can be passed only to the one with the upper bound

### Lower-Bounded Wildcards

| Code | Method compile | Can pass a `List<String>` | Can pass a `List<Object>`
| ------ | ------ | ------ | ------ |
| `addSound(List<?> list) { list.add("quack") }` |  no (unbounded generics are immutable) | Yes | Yes |
| `addSound(List<? extends Object> list) { list.add("quack") }` | no (upper-bounded generics are immutable) | Yes | Yes |
| `addSound(List<Object> list) { list.add("quack") }` | Yes | No (must pass exact match) | Yes | 
| `addSound(List<? super String> list) { list.add("quack") }` |  Yes | Yes | Yes |

- With a lower bound, we are telling Java that the list will be a list of String objects or a list of some objects that are a superclass of String

### Putting It all Together
```java
import java.util.*;
class Playground {
    public static void main(String[ ] args) {
    	List<?> list1 = new ArrayList<A>();
    	List<? extends A> list2 = new ArrayList<A>();
    	List<? super A> list3 = new ArrayList<A>();
    	List<? extends B> list4 = new ArrayList<A>(); // DOES NOT COMPILE
    	List<? super B> list5 = new ArrayList<A>();
    	List<?> list6 = new ArrayList<? extends A>(); // DOES NOT COMPILE
    }
    <T> T method1(List<? extends T> list) {
    	return list.get(0);
    }
    /**
     * Does not compile because the return type isn't actually a type
     */
    <T> <? extends T> method2(List<? extends T> list) {
    	return list.get(0);
    }
    /**
     * Does not compile because within the scope of the method
     * <b>B</b> can represent classes A, B or C.
     * Since B no longer refers to the B class in the method, you can't instantiate it.
     */
    <B extends A> B method3(List<B> list) {
    	return new B();
    }
    void method4(List<? super B> list) {
    }
    /**
     * Does not compile because it tries to mix a method-specific type parameter with a wildcard.
     * A wildcard must have a ? in it.
     */
    <X> void method5(List<X super B> list) {
    	
    }
}
class A {}
class B extends A {}
class C extends B {}
```

## Using List, Set, Maps and Queues
There are four main interfaces in the Java Collections Framework:
- **List**: A list is an ordered collection of elements that allows duplicate entries. Elements in a list can be accessed by an int index.
- **Set**: A set is a collection that does not allow duplicate entries
- **Queue**: A queue is a collection that orders its elements in a specific order for processing. A typical queue process its elements in a first-on, first-out order, but other ordering are possible
- **Map**: A map is a collection that maps keys to values, with no duplicate keys allowed. The elements in a map are key/value pair.

```java
/*The `add()` method inserts a new element into the Collection and returns whether it was successful.*/
boolean add(E element);

/*The `remove()` method removes a single matching value int the Collection and return whether it was successful*/
boolean remove(Object object);

/*Since calling `remove()` with an int uses the index, and indez that doesn't exist will throw an exception (IndexOutOfBoundsException)*/
boolean remove(int index);

boolean isEmpty();
int size();

/*Provides an easy way to discard all elements of the Collection*/
void clear();

/*It checks if a certain value is in the collection.
  This method calls equals() on each element of the ArrayList to see if there are any mathes*/
boolean contains(Object object);
```

### Using the List interface
- You use a list when you want an ordered collection that can contain duplicate entries.
- Items can be retrieved and inserted at specific positions in the list based on an int index much like an array

#### Comparing List Implementation
- An **ArrayList** is like a resizable array. When elements are added, the ArrayList automaically grows.
- A **LinkedList** is special because it implements both List and Queue. The main benefits of a LinkedList are that you can access, add, and remove from the beginning and end of the list in constant time. The tradeoff is that dealing with an arbitrary index takes linear time. This makes a **LinkedList** a good choice when you'll be using it as **Queue**
- **Vector** does the same thing as ArrayList except more slowly due to it is thread-safe, but there is a better way to do that. Vector is really old code
- A **Stack** is a data structure where you add and remove lements from the top of the stack. Think about a stack of paper as an example. Like **Vector**, **Stack** hasn't been used for new code in ages. In fact, **Stack** extends **Vector**. If you need a stack, use an ArrayDeque instead

#### Working with List Methods

| Method | Description |
|--------|-------------|
| void add(E element) | Adds element to end |
| void add(int index, E element) | Adds element at index and moves the rest toward the end |
| E get(int index) | Retrurn element at index |
| int indexOf(Object o) | Returns first matching index or -1 if not found |
| int lastIndexOf(Object o) | Return last matching index or -1 if not found |
| void remove(int index) | Removes element at index and moves the rest toward the front |
| E set(int indexm E e) | Replaces element at index and return original |

### Using the Set interface
- You use a set when you don't want to allow duplicate entries. 
- You aren't concerned with the order in which you see these elements

#### Comparing Set Implementation
- A **HashSet** store its elements in a hash table, it uses the `hashCode()` of the objects to retrieve them more efficiently. The main benefit is that adding elements and checking if an element is in the set both have constant time. The tradeoff is that you lose the order in which you inserted the elements 
- A **TreeSet** store its elements in a sorted tree structure. The main benefit is that the set is always in sorted order. The tradeoff is that adding and checking if an element is present are both O(log n). **TreeSet** implements a special interface called **NavigableSet**, wich lets you slice up the collection

#### Working with Set Methods
TreeSet implements the NavigableSet interfaces.

| Methods | Description|
|---------|------------|
| E lower(E e) | Returns greatest element that is < e, or null if no such element |
| E floor(E e) | Returns greatest element that is <= e, or null if no such element |
| E ceiling(E e) | Returns smallest element that is >= e, or null if no such element |
| E higher(E e) | Returns smallest element that is > e, or null if no such element |

### Using the Queue Interface
- You use a queue when elements are added and removed in a specific order
- A queue is assumed to be FIFO

#### Comparing Queue Implementatios
- **LinkedList** is a double-ended queue, you can insert and remove elements from both the front an dback of the queue. The main benefit of a LinkedList is that it implements both the List and Queue interfaces. The tradeoff is that it isn't as efficient as a "pure" queue.
- **ArrayDeque** is a "pure" double-ended queue. The main benefit is that it is more efficient than a LinkedList. 

### Working with Queue Methods
| Methods | Description | For queue | For stack |
|---------|-------------|-----------|-----------|
| boolean add(E e) | Adds an element to the back of the queue and returns true or throws an exception | Yes | No |
| E element() | Returns next element or throws an exceptio if empty queue | Yes | No |
| boolean offer(E e) | Adds an element to the back of the queue and return whether successful | Yes | No |
| E remove() | Removes and returns next element or throws an exception if empty queue | Yes | No |
| void push(E e) | Adds an element to the front of the queue | Yes | Yes |
| E poll() | Removes and returns next element or return null if empty queue | Yes | No |
| E peek() | Returns next element or return null if empty queue | Yes | Yes |
| E pop() | Removes and returns next element or throws an Exception if empty queue | No | Yes |

Except for `push`, all are in the Queue interface as well, `push` is what makes it a double-ended queue.

### Map 
- You use a map when you want to identify values by a key

#### Comparing Map Implementations 
- **HashMap** stores the keys in a hash table. This means that it uses the `hashCode()` method of the keys to retrieve their values more efficiently. The main benefit is that adding elements and retrieving the element by key both have constant time. The tradeoff is that you lose the order in which you inserted the elements
- A **TreeMap** stores the key in a sorted tree structure. The main benefit is that the keys are always in sorted order. The tradeoff is that adding and checking if a key is present are both O(log n).
- A **Hashtable** is like **Vector** in that it is really old and thread-safe. 

#### Working with Map Methods

| Method | Description |
|--------|-------------|
| void clear() | Remove all keys and values from the map |
| boolean isEmpty() | Returns whether the map is empty |
| int size() | Returns the number of entries(key/values pairs) in the map |
| V get(Object key) | Returns the value mapped by key or null if none is mapped |
| V put(K key, V value) | Adds or replaces key/value pair. Returns previous value or null |
| V remove(Object key) | Removes and returns value mapped to key. Return null if none |
| boolean containsKey(Object key) | Returns whether key is in map |
| boolean containsValue(Object) | Returns value is in map |
| Set<k> keySet() | Return set of all keys |
| Collection<V> values() | Returns Collection of all values |

### Comparing Collection Types
| Type | Can contain duplicate elements? | Elements ordered? | Has keys and values | Must add/remove in specific order? |
|------|---------------------------------|-------------------|---------------------|------------------------------------|
| List | Yes | Yes(by index) | No | No |
| Map | No | No | Yes | No |
| Queue | Yes | Yes(retrieved in defined order) | No | Yes |
| Set | No | No | No | No |

| Type | Java Collections Framework interface | Sorted? | Calls hashCode? | Calls compareTo? |
|------|--------------------------------------|---------|-----------------|------------------|
| ArrayList | List | No | No | No |
| ArrayDeque | Queue | No | No | No |
| HashMap | Map | No | Yes | No |
| HashSet | Set | No | Yes | No |
| Hashtable | Map | No | Yes | No |
| LikendList | Queue and List | No | No | No |
| Stack | List | No | No | No |
| TreeMap | Map | Yes | No | Yes |
| TreeSet | Set | Yes | No | Yes |
| Vector | List | No | No | No |

- Most data sructires allow null
- The data structures that involve sorting do not allow nulls
- TreeSet cannot contain null elements. I also means that TreeMap cannot contain null keys, Null values are OK
- You can not put null in an ArrayDque because methods like poll() use null as a special return value to indicate that the collection is empty
- HashTable doesn't allows null keys or values.

| Wich class do you choose when you want | Answer (single best type) | Reason |
|----------------------------------------|---------------------------|--------|
| to pick the top zoo map off a stack of maps | ArrayDeque | Is of a last-in, first-out data structure, so you need a stack, which is a type of Queue(Stack would also match this description, but it shouldn't be used for new code) |
| to sell tickets to people int the order in which they appear in line and tell them their position in line | LinkedList | Is of a first-in, fisrt-out data structure, so you need a queue. You also needed indexes, and LinkedList is the only class to match both requirements. |
| to write down the first names of all of the elephants so that you can tell them to your friend's three-year-old every time she asks. (The Elephants do not have unique first names) | ArrayList | Since there are duplicates, you need a list rather than a set. You will be accessing the list more often that updating it, since three-year-olds ask the same question over and over, making an ArrayList better than a LinkedList, Vector and Stack aren't used in new code.|
| to list the unique animals that you want to see at the zoo today | HashSet | The keyword is unique. When you see "unique", think "set". Since there were no requirements to have a sorted order or to remember the insertion order, you use the most officient set. |
| to list the unique animals that you want to see at the zoo today in alphabetical order | TreeSet | Since it says "unique", you need a set. This time, you need to sort, so you cannot use a HashSet. |
| to look up animals based on a unique identifier | HashMap | Looking up by key should make you think of a map. Since you have no ordering or sorting requirements, you should use the most basic map. |


## Comparator vs Comparable
- You can also sort objects that you create.
- Java provides an interface called **Comprable**
- If you class implements **Comprable** , it can be used in these data structures that require comparison
- There is also a class called Comparator, which is used to specifiy that you want to use a different order than the object itself provides.

### Comparable 
The **Comparable** is an interface with only one method.
```java
public interface Comparable<T> {
    public int compareTo(T t);
}
```

There are three rules to know:
- The number zero is returned when the current object is equal to the argument to `compareTo()`
- A number less than zero is returned when the current object is smaller than the argument to `compareTo()`
- A number greater than zero is returned when the current object is larger than the argument to `compareTo()`
```java
 public class Animal implements Comparable<T> {
     private int id;
     public int compareTo(Animal a) {
         return id - a.id; // ascending order
         return a.id - id; // descending order
     }
 }
```
- A natural ordering that uses `compareTo()` is said to be consistent with equals if, and only ifm `x.equals(y)` is true whenever `x.compareTo(y)` equals 0
- This is an example of a NOT consistent definition of `compareTo()`
```java
public boolean equals(Object obj) {
    if(!obj instanceof Animal){
        return false;
    }
    Animal animal = (Animal) obj;
    return this.id == animal.id;
}
public int compareTo(Animal a) {
    return name.compareTo(a.name);
}
```
### Comparator 
- Sometimes you want to sort an object that did not implement **Comparable**
- Or you want to sort objects in different ways at different times
- **Comparator** is a funtional interface
```java
public class Duck implements Comparable<Duck> {
    public String name;
    public int weight;

    public Duck(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public int compareTo(Duck d) {
        return name.compareTo(d.name);
    }
    // toString, getters and setters

    public static void main(String[] args) {
        Comparator<Duck> byWeight = new Comparator() {
            public int compare(Duck duck1, Duck duck2) {
                return duck1.weight - duck2.weight;
            }
        };
        List<Duck> ducks = new ArrayList<>();
        ducks.add(new Duck("Quack", 7));
        ducks.add(new Duck("Puddles", 10));
        Collections.sort(ducks);
        System.out.println(ducks); //  [Puddles, Quack]
        Collections.sort(ducks, byWeight);
        Collections.sort(ducks, (d1, d2) -> d1.weight - d2.weight); // lambda expression
        System.out.println(ducks); //  [Quack, Puddles]
    }
}
```

| Difference | Comparable | Comparator |
|------------|------------|------------|
| Package name | java.lang | java.util |
| Interface must be implemented by class comparing | Yes | No |
| Method name in interface | compareTo | compare |
| Number of parameters | 1 | 2 |
| Commom to declare using a lambda | No | Yes |

```java
class Squirrel {

	public String species;
	public int weight;

	Squirrel(String theSpecies) {
		species = theSpecies;
	}

	class MultiFieldComparator implements Comparator<Squirrel> {
		@Override
		public int compare(Squirrel arg0, Squirrel arg1) {
			int result = arg0.species.compareTo(arg1.species);
			if (result != 0)
				return result;
			return arg0.weight - arg1.weight;
		}
	}

	class ChaininComparator implements Comparator<Squirrel> {
		@Override
		public int compare(Squirrel o1, Squirrel o2) {
			Comparator<Squirrel> c = Comparator.comparing(s -> s.species);
			c = c.thenComparing(s -> s.weight);
			return c.compare(o1, o2);
		}
	}
}
```

## Searching and Sorting
- `Collections.sort(list)` doesn't compiles if list not implements the interface `Comparable`
- You can fix this, passing a Comparator like this `Collections.sort(list, (c1, c2) -> c1.id - c2.id`
```java
List<String> names = Arrays.asList("A", "B");
Comparator<String> c = Comparator.reverseOrder();
//Collections.sort(names, c);
int index = Collections.binarySearch(names, "B", c);
System.out.println(index); // -1
```

- If you add a class that not implements Comparable into a `Tree*` Java thows an exception
- We can tell collection that require sorting that you wish to use a specific Comparator
```java
Set<Rabbit> rabbit = new TreeSet<>(new Comparator<Rabbit>() {
    public int compare(Rabbit r1, Rabbit r2) {
        return r1.id - r2.id;
    }
});
rabbit.add(new Rabbit());
```

## Additions in Java 8
We will show you how to use the new `removeIf()`, `forEach()`, `merge()`, `computeIfPresent()`, and `computeIfAbsent()`

### Using Methods References 
- **Method references** are a way to make the code shorter by reducing some of the code that can be inferred and simply mentioning the name of the method
- The :: operator tells Java to pass the parameters automatically 

```java
Comparator<Duck> byWeight = (d1, d2) -> DuckHelper.compareByWeight(d1, d2);
Comparator<Duck> byWeight = DuckHelper::compareByWeight; // Returns a functional interface and not an int
```

There are four formats for method references:
- Static methods
- Instance methods on a particular instance
- Instance methods on an instance to be determined at runtime
- Constructors

```java
//Static method
// Java is inferring information from the context
Consumer<List<Integer>> methodRef1 = Collections::sort;
Consumer<List<Integer>> lambda1 = l -> Collections.sort(1);

// Instance method on a specific instance
Stirng str = "abc";
Predicate<String> methodRef2 = str::startWith;
Predicate<String> lambda2 - s -> str.startWith(s);

// Instance methods without knowing the instance in advance
Predicate<String> methodRef3 = String::isEmpty; // It looks like a static method, but it isn't. 
Predicate<String> lambda3 = s -> s.isEmpty();

// Contructor reference
Supplier<ArrayList> methodRef4 = ArrayList::new; // Is a special type of method reference that uses new instead of a method
Supplier<ArrayList> lambda4 = () -> new ArrayList();
```

### Removing Conditionally
We had the ability to remove a specified object from a collection or a specified index from a list. Now we can specify what should be deleted using a block of code.
```java
boolean removeIf(Predicate<? super E> filter)
List<Stirng> list = new ArrayList<>();
list.add("Magician");
list.add("Assistant");
System.out.println(list); // Magician, Assistant
list.removeIf(s -> s.startsWith("A")); // You can't replace with a method reference because startWith takes a parameter that isn't "s"
System.out.println(list); // [Magician]
```

### Updating All Elements

```java
void replaceAll(UnaryOperator<E> o)

List<Integer> cats = Arrays.asList(1,2,3);
list.replaceAll(x -> x*2);
System.out.println(list); // [2, 4, 6]

```
- The result replaces the current value of that element.
- It uses a UnaryOperator, which takes one parameter and returns a value of the same type.

### Looping through a Collection

```java
List<String> cats = Arrays.asList("A", "B");
cats.forEach(s -> System.out.println(s));
cats.forEach(System.out::println);
```

## Using New Java 8 Maps APIs
- Sometime you need to update the value for a specfifc key in the map. 
- The first way is to replace the existing value unconditionally
- There's another method, called `putIfAbsent()`, that you can call if you want to set a value in the map, but this method skips it if the value is already set to a non-null value.
```java
Map<String, String> map = new HashMap<>();
map.put("Jenny", "Bus Tour");
map.put("Tom", null);

map.putIfAbsent("Jenny", "Tram");
map.putIfAbsent("Tom", "Tram");
map.putIfAbsent("Sam", "Tram");
// Tom=Tram, Jenny=Bus Tour, Sam=Tram
```

### merge
- The `merge()` method allows adding logic to the problem of what to choose.
- Use a functional interfaces called a BiFunction.
- The `merge()` method also has logic for what happens if nulls or missing keys are involved
- When the mapping function is called and return null, the key is removed from the map.
```java
BiFunction<String, String, String> biFunction = (s1, s2) -> s1.length() > v2.length() ? s1 : s2;
Map<String, String> favorites = new HasMap<>();
favorites.put("Jenny", "Bus Tour");
favorites.put("Tom", "Tram");
favorites.put("Sam", "null");

String jenny = favorite.merge("Jenny", "Skyride", biFunction);
String tom = favorite.merge("Tom", "Skyride", biFunction);
String sam = favorite.merge("Sam", "Skyride", biFunction);
// Jenny=Bus Tour, Tom=Skyride, Sam=Skyride

String x = favorite2.merge("Jenny", "Skyride", (s1, s2) -> null);
String x = favorite2.merge("Tom", "Skyride", (s1, s2) -> null);
// Tom=Skyride, Sam=Skyride
```

### computeIfPresent and computeIfAbsent
computeIfPresent
- The `computeIfPresent()` metohd calls the BiFunction if the requested key is found
- The function interface is a BiFunction again. However, this time the key and value are passed rather than two values. Just like with `merge()`, the return value is the result of what changed in the map or null if that doesn't apply
- If the mapping function is called and returns null, the key is removed from the map

computeIfAbsent
- For `computeIfAbsent()`, the functional interface runs only when the key isn't present or is null
- Since there is no value already in the map, a `Function` is used instead of a `BiFunction`. Only the key is passed as input
- If the mapping function is called and returns null, the key is never added to the map in the first place

```java
// computeIfPresent
Map<String, Integer> maps = new HashMap<>();
BiFunction<String, Integer, Integer> mapperPresent =  (k, v) -> v+1;
maps.put("A", 1);
maps.put("B", null);

maps.computeIfPresent("A", mapperPresent); // A=2
maps.computeIfPresent("B", mapperPresent); // null
maps.computeIfPresent("C", mapperPresent); // null
maps.computeIfPresent("A", (k, v) -> null); // null

// computeIfAbsent
Map<String, Integer> maps2 = new HashMap<>();
Function<String, Integer> mapperPresent =  (k) -> 1;
maps.put("A", 1);
maps.put("B", null);

maps.computeIfAbsent("A", mapperPresent); // A=1
maps.computeIfAbsent("B", mapperPresent); // B=1
maps.computeIfAbsent("C", mapperPresent); // C=1
maps.computeIfAbsent("A", (k) -> null); // A=1
```

| Scenario | merge | computeIfAbsent | computeIfPresent |
|----------|-------|-----------------|------------------|
|Key already in map | Put a value of key | Nothing | Put a value of key |
|Key not already in map | Add a key | Add a key | Nothing |
|Functional Interfaces used | BiFunction (Takes exiting value and new value. Returns new value) | Function(Takes key and existing value. Returns new value) | BiFunction(Takes key and returns new value) |


