# Design Patters and Principles
- [x] Develop code that declares, implements and/or extends interfaces and use the `@Overrides` annotation
- [x] Create and use Lambda expressions
- [X] Use the built-in interfaces included in the java.util.function package such as Predicate, Consumer, Function and Supplier
- [X] Implement encapsulation
- [X] Implement inheritance including visibilty modifiers and composition
- [X] Implement polymorphism
- [X] Create and use singleton classes and immutable classes

# Exam Essentials
- Be able to write code that declares, implements, and/or extends interfaces
- Know how to create and recignize a functional interface
- Be able to write valid lambda expressions
- Undesrtand Polymorphism
- Understand the importance of design principles and design patterns
- Know how to implement encapsulation
- Be able to apply the is-a and has-a tests
- Be able to apply object composition and distinguish it from inheritance
- Be able to apply creational patterns including the singlenton pattern and the immutable object pattern


## Designin an Interface
- An interface is an abstract data type, may also include constant `public static final` variables, default methods and `static` methods
- The compiler automaticlally adds `public` to all interface methods and `abstract` to all non-static and non-default methods.
- The class implementing the interface must provide the proper modifiers.
- Interface cannont extends classes, nor can classes extends interfaces.
- Interfaces may by also not be marked `final` or instantiated directly.

## Introducing Functional Programming
- Java defines a functional interface as an interface that contiains a single `abstract method`.
- Functional interfaces are used as the bases for lambda expressions in functional programming.
- If a class marked with the `@FunctionalInterface` annotations contains more than one abstrct method, or no abstract methods at all, then the compiler will detect this error and not compile.
- An interface can declared a static methods or default methods that if It haves only one abstract method continues been a Functional Interface.

```java
@FunctionalInterface
public interface Sprint {
    public void sprint(Animal animal);
}
```

## Implementing Functional interfaces with Lambda
- A `lambda expression` is a block of code that gets passed around, like an anonymous method.
```java
a -> a.canHop()
```
- When one parameter has a data tpye listed, though, all parameters must provide a data type.

## Applying the Predicate Interface
- It's in the package `java.util.function` 
```java
public interface Predicate<T> {
    public boolean test(T t);
}
```
## Implement polymorphism
- **Polymorphism*** is the ability of a single interface to suport multiple underlying forms
- This allows multiple types of objects to be passed to a single method or class. 
- If you use a variable to refer to an object, then only the methods or variables that are part of the variable's reference type can be called without an explicit cast.

## Distinguishing between an Object and a Reference
- In Java, all objects are accessed by reference, so as a developer you never have direct access to the memory of the object itself.
- Regardless of the type of the reference that you have for the object in memory, the object itself doesn't change.

1. The type of the object determines which properties exist within the object in memory
2. The type of the reference to the object determines which methods and variables are accessible to the Java program.

## Casting Object References
- Casting an object from a subclass to a superclass doesn't require an explicit cast
- Casting an object from a superclass to a subclass requires an explicit cast.
- The compiler will not allow casts unrelated types
- Even when the code compiles without issue, an exception may be thrown at runtime if the object being cast is not actually an instance of the class

## Undestanding Design Principles
In general, following good design principles lead to:
- More logical code
- Code that is easier to understand 
- Classes that are easier to reuse in other relationships and applications
- Code that is easier to maintain and that adapts more readily to changes in the applicationi requirements

### Encapsulating Data
- `Encapsulation` is the idea of combining fields and methods in a class such that the methods operate on the data, as opposed to the users of the class accessing the fields directly.
- An `invariant` is a property or truth that is maintained even after the data is modified

### Creating JavaBeans 
A `JavaBean` is a design principle for encapsulation data in an object in Java
- Properties are private
- Getter for non-boolean properties begins with `get`
- Getter for boolean properties may begin with `is` or `get`
- Setter methods begin with `set`
- The method name must have a prefix of `set/get/is` followed by the first letter of the property in uppercase and followed by the rest of the property name

### Applying the Is-a Relationship
- The fundamental result of the `is-a` principle is that if A is a B, then any instance of A can be trated like an instance of B.

### Applying the Has-a Relationship
- We refere to the `has-a relationship` as the property of an object having a named data object or primitive as a member. It is also known as the object composition test.
- If a parent has-a object as a protected or public member, then any child of the parent must also have that object as a member. Note that this does noit hold true for private members defined in parent classes, beacuse private members are not inherited in Java.

### Composing Objects
- In object-riented design, we refer to `object composition` as the porperty of constructing a class using references to other classes in order to REUSE the funcionality of the other classes.
- One of the advantages of object composition over inheritance is that it tends to promote greater code reuse. 
- By using object composition, you gain access to other classes and methods that would be difficult to obtain via Java's single-inheritance model

## Working with Design Patterns
A `design pattern` is an established general solution to a commonly ocurring software development problem.

### Applying the Singlenton Pattern
The singlenton pattern is a creational pattern focused on creating only one instance of an object in memory within an application, sharable by all classes and threads within the application.
- By marking the constructor private, we have implicitly marked the class `final`.

### Applying Lazy Instantiation to Singletons
- Creating a reusable object the first time it is requested is a software design pattern known as `lazy instantiation`
- Reduce memory usage and improves performance when an application starts up
- The downside of lazy instantiation is that users may see a noticeable delay the first time a particular type of resource is needed

### Creating Unique Singletons
- Make a constructor as `private`
- We guaranteed that the object is created once within the singlenton class itself by using the `final` modifier and the `static` reference.
- `Thread safety` is the property of an object that guarantees safe execution by multiple threads at the same time.
```java
public static synchronized Singleton getInstance() {
    if(instance == null) {
        instance = new A();
    }
    return instance;
}
```

## Creating Immutable Objects
- The `immutable object pattern` is a creational pattern based on the idea of creating objects whose state does not change after they are created and can be easily shared across multiple classes.
- Immutable objects go hand and hand with encapsulation, but that no setter methods exist that modify the object.

### Applying an Immutable Strategy
- Use a constructor to set all properties of the object
- Mark all of the instance variables `private` and `final`
- Don't define any setter methods
- Don't allow referenced mutable objects to be modified or accessed directly
- Prevent methods from being overridden

This is a example of the not immutable object
```java
import java.util.*;
final class Playground {
    private final List<String> favoriteFoods;
    public Playground(List<String> favoriteFoods) {
        this.favoriteFoods = favoriteFoods;
    }
    public int getTotal() {
        return this.favoriteFoods.size();
    }
    public static void main(String[ ] args) {
        List<String> favoriteFoods = new ArrayList<String>();
        favoriteFoods.add("Hello");
        Playground p = new Playground(favoriteFoods);
        System.out.println(p.getTotal()); // 1
        favoriteFoods.add("Hello");
        System.out.println(p.getTotal()); // 2
    }
}
```
### "Modifying" and Immutable Object
We can create new immutable objects that contain all of the same information as the original object plus whatever we wanted to change.

## Using the Builder Pattern
- The `builder pattern` is a creational pattern in which parameters are passed to a builder object, often through method chaining, and object is generated with a final build call. 
- We can modify this class as we build it, and the result of the build method will be an immutable object.
- All of the setter methods return an instance of builder object `this`

## Creating Objecs with the Factory Pattern
- The factory pattern sometimes referred to as the factory method pattern, is a creational pattern based on the idea of using a factory class to produce instances of objects based on a set of input parameters
- It's similiar to the builder pattern, although it is focused on supporting class polymorphism
- Factory patterns are often, although not always, implementd using static methods that return objects and do not required a pointer to an instance of the Factory class;


