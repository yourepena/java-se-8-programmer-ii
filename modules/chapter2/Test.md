1. [x] C, E
2. [x] E
3. [x] A, D
4. [x] A, D
5. [x] B, E, F
6. [x] A
7. [ ] C, E, G, `H`  
8. [X] C, F
9. [X] D, F
10. [x] D, Duplicate default methods named walk with the parameters () and () are inherited from the types B and A
11. [X] A, F
12. [x] C
13. [ ] B, C, `E`
14. [ ] A, `B` , D, E
15. [x] B, C
16. [x] F
17. [x] B, C, F
18. [X] A, B, E
19. [ ] `B`,`D`,`E`
20. [X] A


- Immutable Object must be final class due to It' prevent that the methods of the class from being overriden
- The instance variable and access method are dont required named to `instance` or `getInstance`
- Create read-only objects that are thread-safe is a propertyf of the immutable objects.
- Caching data is one of the most commom uses of the singlenton pattern
- While the singlenton pattern may use lazy initialitation, is it not used to unsure that objects are lazyly instantiated
- Singlenton Patter provide central access to application configuration data
- Singlenton Patter manage write access to a log file
