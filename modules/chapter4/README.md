# Functional Programming
- Generic and Collections
    - [x] Collections Streams and Filters
    - [x] Iterate using forEach methods of Streams
    - [x] Describe Stream interface and Stream pipeline
    - [x] Use method references with Stream
- Lambda Built-In Functional Interfaces
    - [x] Use the built-in interface included in the `java.util.function` such as Predicate, Consumer, Function and Supplier
    - [X] Develop code that uses primitive versions of functional interface
    - [x] Develop code that uses binary versions of functional interface
    - [x] Develop code that uses the UnaryOperator interface
- Java Stream API
    - [x] Develop code to extract data from object using peek() and map() methods including primitive versions of the map() method
    - [x] Search for data by using search methods of the Stream classes including findFirst, findAny, anyMatch, allMatch and noneMatch
    - [x] Develop code that uses the Optional class
    - [x] Develop code that uses Stream data methods and calculation methods
    - [x] Sort a collection using Stream API
    - [x] Save results to a collection using the collect method and group/partition data using the Collectors class
    - [x] Use of merge() and flatMap() methods of the Stream API

## Using variables in Lambdas

### Working with Built-In Functional Interfaces

| Functional Interfaces | # Parameters | Return Type | Single Abstract Method |
|-----------------------|--------------|-------------|------------------------|
| Supplier<T> | 0 | T | get |
| Consumer<T> | 1 (T) | void | accept |
| BiConsumer<T, U> | 2 (T, U) | void | accept |
| Predicate<T> | 1 (T) | boolean | test |
| BiPredicate<T, U> | 2 (T, U) | boolean | test |
| Function<T, R> | 1 (T) | R | apply |
| BiFunction<T, U, R> | 2 (T, U) | R | apply |
| UnaryOperator | 1 (T) | T | apply |
| BinaryOperator | 2 (T, T) | T | apply |

#### Implementing Supplier
- A **Supplier** is used when you wan to generate or supply values without taking any input
- Everytime that we call `get()` a new instance from a value T

```java
Supplier<LocalDate> s1 = LocalDate::now;
Supplier<LocalDate> s2 = () -> LocalDate.now();

Supplier<StringBuilder> s3 = () -> StringBuilder::new;
Supplier<StringBuilder> s4 = () -> new StringBuilder();
```

#### Implementing Consumer and BiConsumer
- You use a **Consumer** when you want to do something with a parameter but not return anything. 
- **BiConsumer** does the same thing except that it takes two parameters, and they don't have to be the same type.

```java
Consumer<String> c1 = System.out::println;
Consumer<String> c2 = x -> System.out.println(x);

Map<String, Integer> map = new HashMap<>();
BiConsumer<String, Integer> b1 = map::put;
BiConsumer<String, Integer> b1 = (k,v ) -> map.put(k, v);
```

#### Implementing Predicate and BiPredicate
- **Predicate** is often used when filtering or matching or with `removeIf()`
- A **BiPredicate** is just like a Predicate except that it takes two parameters instead of one

```java
Predicate<String> p1 = String::isEmpty;
Predicate<String> p2 = x -> x.isEmpty();

BiPredicate<String, String> p3 = String::startsWith;
BiPredicate<String, String> p4 =  (string, prefix) -> string.startsWith(prefix);
```

#### Implementing Function and BiFunction
- A **Function** is responsible for turning one parameter into a value of a potentially different type and returning it
- Similarly, a **BiFunction** is responsaible for turning two parameters into a value and returning it.

```java
Function<String, Integer> f1 = String::length;
Function<String, Integer> f2 = s -> s.length();

BiFunction<String, String, String> f3 = String::concat;
BiFunction<String, String, String> f4 = (s1, s2) -> s1.concat(s2);
```

#### Implementing UnaryOperator and BinaryOperator
- **UnaryOperator** and **BinaryOpeator** are a special case of a function require all type parameters to be the same type.
- **UnaryOperator** transforms its value into one of the same type.
- **BinaryOpeator** merges two values into one of the same type

```java
UnaryOperator<String> u1 = String::toUpperCase;
UnaryOperator<String> u2 = x -> x.toUpperCase();

BinaryOperator<String> b1 = String::contact;
BinaryOperator<String> b2 = (a, b) -> a.contact(b);
```

### Checking Functional Interface
- Returns a String without taking any parameter -> Supplier (Consumer needs 1 parameter)
- Returns a Boolean and takes a String -> Function (Because predicate returns a primitive boolean)
- Returns an Integer and takes two Integers -> BinaryOperator or BiFunction

```java
// If returns boolean tell us that it is a Predicate or Function, but the generics declaration has only one parameter
___________<List> ex1 = x -> "".equals(x.get(0)); 

// It doesn't returns anything and take one parameter, so it's an Consumer
___________<Long> ex2 = (Long l) -> System.out.println(l);

// Return a boolean, but takes 2 parameters, so it is a BiPredicate
___________<String, String> ex3 = (s1, s2) -> false;

```

### Return an Optional
- An **Optional** is created using a factory.
- You can either request an empty **Optional** or pass a value for the **Optional** to wrap
- If we didn't do the check and the **Optional** is empty we'd get an exception sice there is no value inse the **Optional**
```java
pubic static Optional<Double> average(int scores...) {
    if(scores.lenght == 0 ) {
        return Optional.empty();
    }
    int sum = 0;
    for(int score : scores) sum += score;
    return Optional.of((double) sum/scores.length);
}

Optional<Double> opt = average(90, 100);
if(opt.isPresent()) {
    System.out.println(opt.get());
} else {
    System.out.println(opt.get()); // NoSuchElementException
}
```
| Method | When Optional Is Empty | When optional Contains a Value |
|--------|------------------------|--------------------------------|
| get() | Throws an exception | Returns value |
| ifPresent(Consumer c) | Does nothing | Calls consumer c with value |
| isPresent() | Returns false | Returns  true |
| orElse(T other) | Returns other parameter | Returns value |
| orElseGet(Supplier s) | Returns result of calling Supplier | Returns value | 
| orElseThrow(Supplier s) | Throws exception created by calling Supplier | Returns value |


```java
Optional<Double> opt = average(90, 100);
opt.ifPresent(System.out::println);

Optional<Double> opt2 = average();
opt2.orElse(Double.NaN);
opt2.orElseGet(() -> Math.random());
opt2.orElseThrow(() -> new IllegalStateException());

```

## Using Streams
- A **stream** in Java is a sequence of data. A **stream pipeline** is the operations that run on a stream to produce a result.
- Many things can happen in the assembly line stations along the way. In programming, these are called **stream operations**
- There are three parts to a stream pipeline:
    - ***Source***: Where the stream comes from
    - ***Intermediate operatins***: Transforms the stream into another one. 
    - ***Terminal operation***: Actually produces a result. Since streams can be used only once, the stream is no longer valid after a terminal operation completes

| Scenario | For Intermediate Operations? | For terminal Operations? |
|----------|------------------------------|--------------------------|
| Required part of a useful pipeline? | No | Yes |
| Can exist multiple times in a pipeline? | Yes | No |
| Return type is a stream type? | Yes | No |
| Executed upon method call? | No | Yes |
| Stream valid after call? | Yes | No |

### Creating Stream Sources 
In Java, the **Stream** interface is in the `java.util.stream` package.

```java
Stream<String> empty = Stream.empty();
Stream<Integer> singleElement = Stream.of(1);
Stream<Integer> fromArray = Stream.of(1, 2, 3);

List<String> list = Arrays.asList("a", "b", "c");
Stream<String> fromList = list.stream();
Stream<String> fromListParallel = list.parallelStream();

Stream<Double> randoms = Stream.generate(Math::random); // Infinite stream
Stream<Double> oddNumbers = Stream.iterate(1, n -> n + 2); // Infinite stream
```

### Using Commom Terminal Operations
- **Reductions** are a special type of terminal operation where all of the contents of the stream are combined into a single primitive or Object.

| Method | What Happens for Infinite Streams | Return Value | Reduction |
|--------|-----------------------------------|--------------|-----------|
| allMatch()<br>/anyMatch()<br>/noneMatch() | Sometime terminates | boolean | No |
| collect() | Does not terminate | Varies | Yes |
| count() | Does not terminate | long | Yes |
| findAny()/findFirst() | Terminate | Optional<T> | No |
| forEach() | Does not terminate | void | No |
| min()/max() | Does not terminate | Optional<T> | Yes |
| reduce() | Does not terminate | Varies | Yes |

#### count()
- The `count()` method determines the number of elements in a finite stream
- For an infinity stram, it hangs.
- Count is a reduction because it looks at each element in the stream and returns a single value.

```java 
log count()
Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
System.out.println(s.count());
```

#### min() and max()
- The `min()` and `max()` methods allow you to pass a custom comparator and find the smallest or largest value in a finite stream according to that sort order
- Like `count()` `min()` and `max()` hang on an inifinite stram bacause they cannot be sure that a smaller or larger value
- Both methods are reductions because they return a single value after looking at the entire stream.
- Since the stream is empty, the comparator is never called and no value is present in the optional

```java 
Optional<T> min(<? super T> comparator);
Optional<T> max(<? super T> comparator);
Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
Optional<String> min = s.min((s1, s2) -> s.length() - s2.length());
min.ifPresent(System.out::println);
```

#### findAny() and findFirst()
- The `findAny()` and `findFirst()` methods return an element of the stream unless the stream is empty.
- If the stream is emppty they return an empty Optional.
- This methods works with an infinite stream
- FindAny() is useful when you are working with a parallel stream.
- These methods are terminal operations but not reductions, the reason is that they sometimes return without processing all of the elements.

```java 
Optional<T> findAny();
Optional<T> findFirst();
Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
Stream<String> infinite = Stream.generate(() -> "chimp" ));
s.findAny().ifPresent(System.out::print);//monkey
infinite.findAny().ifPresent(System.out::print)//chimp
```

### allMatch(), anyMatch() and noneMatch()
- They search a strem and return information about how the stream pertains to the predicate
- These may or may not terminate for infinite streams
- They are not reductions because they do not necessarily look at all of the elements.


```java 
boolean anyMatch(Predicate <? super T> predicate);
boolean allMatch(Predicate <? super T> predicate);
boolean noneMatch(Predicate <? super T> predicate);

List<String> list = Arrays.asList("monkey", "2", "chimp");
Stream<String> infinite = Stream.generate(() -> "chimp" ));
Preditcate<string> pred - x -> Character.isLetter(x.charAt(0));
System.out.println(list.stream().anyMatch(pred)); // true
System.out.println(list.stream().allMatch(pred)); // false
System.out.println(list.stream().noneMatch(pred)); // false
System.out.println(infinite.anyMatch(pred)); // true
```
#### foreach
- A looping construct is available
- As expected, calling `forEach()` on an infinite stream does not terminate.
- Since there is no return value, it is not a reduction
- Streams cannot use a traditional for loop to run because they don't implement the Iterable interface

#### reduce()
The `reduce()` method combines a stream into a single object. 

```java
T reduce(T identity, BinaryOperator<T> accumulator);
Optional<T> reduce(BinaryOperator<T> accumulator);
<U> U reduce(U identity, BiFunction<U, ? super T,U> accumulator, BinaryOperator<T> combiner);

Stream<String> stream = Stream.of("w", "o", "l", "f");
String word = stream.reduce("", (s, c) -> s + c);
String word = stream.reduce("", String::concat);
System.out.println(word);//wolf;
```

- In many cases, the identity isn't really necessary, so Java lets us omit it.
- When you dont't specify an identity, an Optional is returned because there might not be any data
- There are three choices for what is in the Optional:
    - If the stream is empty, an empty Optional is returned
    - If the stream has one element, it is returned.
    - If the stream has multuple elements, the accumulator is applied to comine them

```java
BinaryOperator<Integer> op = (a, b) -> a * b;
Stream.empty().reduce(op).ifPresent(sysout); // no output
Stream.of(3).reduce(op).ifPresent(sysout); // 3
Stream.of(3, 5, 6).reduce(op).ifPresent(sysout); // 90
```

- The third method signature is used when we are processing collections in parallel
- It allos Java to create inermediate reductions and the combine them at the end

```java
BinaryOperator<Integer> op = (a, b) -> a * b;
Stream<Integer> stream = Stream.of(3, 5, 6);
sysout(stream.reduce(1, op, op)); // 90
```

#### collect 
- The `collect()` method is a special type of reduction called a mutbale reduction.
- It is more efficient than a regular reduction because we use the same mutable object while accumulating.

```java
<R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner)
<R, A> R collect(Collector<? super T, A, R> collector)
```
The first signature, which is used when we want to code specifically how collecting should work

```java
Stream<String> stream = Stream.of("w", "o", "l", "f");
StringBuilder word = stream.collect(StringBuilder::new, StringBuilder::append, String::append);
TreeSet<Stirng> set = stream.collect(TreeSet::new, TreeSet::add, TreeSet::addAll);

TreeSet<String> set = stream.collect(Collectors.toCollection(TreeSet::new));
Set<String> set = stream.collect(Collectors.toSet());
```

### Using Common Intermediate Operations

- Unline a terminal operation, intermediate operations deal with inifinite streams simply by returning an infinite stream.
- The assembly line worker doesn't need to worry about how many more elements are coming trhough and instead can focus on the current element

### filter()

- The filter() method returns a Stream with elements that match a given expressions

```java
Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
s.filter(x -> x.startsWith("m")).forEach(System.out::print); // monkey
```

### distinct()

- The `distinct()` method returns a stream with duplicate values removed.
- The duplicates do not need to be adjacent to be removed.
- Java calls `equals()` to determine whether the objects are the same

```java
Stream<String> s = Stream.of("monkey", "gorilla", "gorilla", "bonobo");
s.distinct().forEach(System.out::print); 
```

### limit() and skip() 

- The `limit()` and `skip()` methods make a Stream smaller
- They could make a finite stream smaller, or they could make a finite stream out of an infinite stream

```java
Stream<Integer> s = Stream.iterate(1, n -> n + 1);
s.skip(5).limit(2).forEach(System.out::print); //67
```

### map()

- The `map()` method cretes a one-to-one mapping from the elements in the stream to the elements of the next step in the stream.

```java
Stream<String> s = Stream.of("monkey", "gorilla", "bonobo");
s.map(String::lenght).forEach(System.out::print); // 676
```

### flatMap()

- The `flatMap()` method takes each element in the stream and makes any elements it contains top-level elements in a single stream
- This is helpful when you want to remove empty elements from a stream or you want to combine a stream of lists.

```java
List<string> zero = Arrays.asList();
List<string> one = Arrays.asList("Bonobo");
List<string> two = Arrays.asList("Mama", "Gorila", "Baby Gorila");
Stream<List<String>> animals = Stream.of(zero, one, two);
animals.flatMap(l -> l.stream()).forEach(System.out::println);
```

### sorted()

- The `sorted()` method returns a stream with the elements sorted.
- When you try to sort an infinite stream an Exception is throw
- Just like sorting arrays, Java uses natural ordering unless we specify a comparator

```java
Stream<String> s = Stream.of("a", "b");
s.sorted(Comparator.reverseOrder()).forEach(System.out::print) // b, a
```

### peek()

- The `peek()` method is our final intermediate operation.
- It is useful for debugging because it allows us to perform a stream operation without actually changin the stream.
- Be careful because `peek()` can modifies the data structure that is used in the stream, wich causes the result of the stream pipeline to be differente than if the ppek wasn't present.


### Putting Together the Pipeline 


- Stream allow you to use chaining and express what you want to accomplish rather than how to do so. 

```java
// We wanted to get the first two names alphabetically that are four characters long
List<String> list = Arrays.asList("Toby", "Anna", "Lery", "ALex");
list.stream().filter(n -> n.length() == 4).sorted().limit(2).forEach(System.out::println);
```

| Option | Works for Infinite Stream? | Destructive to Stream? |
|--------|----------------------------|------------------------|
| s.forEach(System.out::println); | No  | Yes |
| System.out.println(s.collect(Collectors.toList())); | No | Yes |
| s.peek(System.out:println).count(); | No | No |
| s.limit(5).forEach(System.out::println); | Yes | Yes |

## Working with Primitives 

- Java provides a method to calculate the average on the stream classes for primiteves

```java
Stream stream = Stream.of(1, 2, 3);
System.out.println(stream.reduce(0, (s, b) -> s + b));

IntStream intStream  = IntStream.of(1, 2, 3);
System.out.println(intStream.mapToInt(x -> x).sum());

OptionalDouble avg = intStream.average();
System.out.println(avg.getAsDouble());
```

### Creating Primitive Streams
Here are three types of primitive streams:
- IntStream: Used for the primitive types int, short, byte and char.
- LongStream: Used for the primitive type long
- DoubleStream: Used for the primitive types double and float.

Some of methods for creating a primitive stream are equivalent to how we created the source for a regular Stream.
- `empty()`, `of()` -> finite streams
- `generate()`, `iterate` -> infinite streams

Java provides a method that can generate a reange of numbers
```java
IntStream range = IntStream.range(1, 6); // not including the number 6
range.forEach(System.out::println);
```

This time we expressed that we want a closed range, or an inclusive range
```java
IntStream range = IntStream.rangeClosed(1, 5);
range.forEach(System.out::println);
```

| Source Stream Class | To Create Stream | To Create Double Stream | To Create IntStream | To Create LongStream |
|---------------------|------------------|-------------------------|---------------------|----------------------|
| Stream | map | mapToDouble | mapToInt | maptoLong |
| DoubleStream | mapToObj | map | mapToInt | mapToLong |
| IntStream | mapToObj | mapToDouble | map | mapToLong |
| LongStream | mapToObj | mapToDouble | mapToInt | map |

| Source Stream Class | To Create Stream | To Create Double Stream | To Create IntStream | To Create LongStream |
|---------------------|------------------|-------------------------|---------------------|----------------------|
| Stream | Function | ToDoubleFunction | ToIntFunction | ToLongFunction |
| DoubleStream | DoubleFunction | DoubleUnaryOperator | DoubleToIntFunction | DoubleToLongFunction |
| IntStream | IntFunction | IntToDoubleFunction | IntUnaryOperator | IntToLongFunction |
| LongStream | LongFunction | LongToDoubleFunction | LongToIntFunction | LongUnaryOperator |

### Using Optional with Primitive Streams

- OptionalDouble is for a primitive and Optional<Double> is for the Double wrapper class.
- The only difference is that we called `getAsDouble()` rather than `get()`
- Also, orElseGet() takes a DoubleSupplier instead of a Supplier
- A number of stream methods return an optional such as `min()` or `findAny()`. These each return the corresponding option type
- The `sum(`) method odes not return an optional, if you try ti add up an empty stream, you get zero.
-  The `avg(`) method always returns an OptionalDouble.

| | OptionalDouble | OptionInt | OptionalLong |
|--|---------------|-----------|--------------|
| Getting as a primitive | getAsDouble() | getAsInt() | getAsLong() |
| orElseGet() | DoubleSupplier | IntSupplier | LongSupplier |
| Return type of max() | OptionalDouble | OptionalInt | OptionalLong |
| Return type of sum() | double | int | long |
| Return type of avg() | OptionalDouble | OptionalDouble | OptionalDouble

```java
LongStram longs = LongStream.of(5, 10);
long sum = longs.sum(); // 15

DoubleStream doubles = DoubleStream.generate(() -> Math.PI);
OptionDouble min = doubles.min() // runs inifinitely
```

### Summarizing Statistics
- We asked Java to perform many calculation about the stream (minimun, maximum, average, size)

```java
IntSummaryStatistics stats = ints.summaryStatistics();
if(stats.getCount() == 0) {
    throw new RuntimeException();
}
return stats.getMax()-stats.getMin()
```

### Lerning the Functional Interface for Primitives

BooleanSupplier is a separate type. It has one method to implement:

```java
boolean getAsBoolean()
```

| Functional Interfaces | # Parameters | Return Type | Single Abstract Method |
|-----------------------|--------------|-------------|------------------------|
| DoubleSupplier<br>IntSupplier<br>LongSupplier | 0 | double<br>int<br>long | getAsDouble<br>getAsInt()<br>getAsLong |
| DoubleConsumer<br>IntConsumer<br>LongConsumer | 1(double)<br> 1(int)<br>1(long) | void | accept |
| DoublePredicate<br>IntPredicate<br>LongPredicate | 1(double)<br> 1(int)<br>1(long) | boolean | test |
| DoubleFunction<R><br>IntFunction<R><br>LongFunction<R> | 1(double)<br> 1(int)<br>1(long) | R | apply |
| DoubleUnaryOperator<br>IntUnaryOperator<br>LongUnaryOperator | 1(double)<br> 1(int)<br>1(long) | double<br>int<br>long | applyAsDouble<br>applyAsInt<br>applyAsLong |
| DoubleBinaryOperator<br>IntBinaryOperator<br>LongBinaryOperator | 2(double, double)<br>2(int, int)<br>2(long, long) | double<br>int<br>long | applyAsDouble<br>applyAsInt<br>applyAsLong |

There are a few things to notice that are different:
- Generics are gone from some of the interfaces, since the type name tell us what primitive type is involved
- The single abstract method is often, but not always, renamed to reflect the primtive type involved.
- `BiConsumer`, `BiPredicate` and `BiFunction` are not in Table. The API designers sutck to the most common operations. For primitives, the functions with two different type parameters just aren't used often. 

***Primitive-specific funcional interfaces***

| Functional Interfaces | # Parameters | Return Type | Single Abstract Method |
|-----------------------|--------------|-------------|------------------------|
| ToDoubleFunction<T><br>ToIntFUnction<T><br>ToLongFunction<T> | 1(T) | double<br>int<br>long | applyAsDouble<br>applyAsInt<br>applyAsLong |
| ToDoubleBiFunction<T, U><br>ToIntBiFunction<T, U><br>ToLongBiFunction<T, U> | 2 (T, U) | double<br>int<br>long | applyAsDouble<br>applyAsInt<br>applyAsLong |
| DoubleToIntFunction<br>DoubleToLongFunction<br>IntToDoubleFunction<br>IntToLongFunction<br>LongToDoubleFunction<br>LongToIntFunction<br> | 1(double)<br>1(double)<br>1(int)<br>1(int)<br>1(long)<br>1(long) | int<br>long<br>double<br>long<br>double<br>int | applyAsInt<br>applyAsLong<br>applyasDouble<br>applyAsLong<br>applyasDouble<br>applyAsInt |

## Working with Advanced Stream Pipeline Concepts 
You will se the relationship between srteams and the underlying data, chaining Optional and grouping collectors.

### Linking Streams to the Underlying Data
- Strams are lazily evaluated, this means that the stream isn't actually created in the `.stream()`, the stream pipeline actually runs in the `.count()`

```java
List<String> list = new ArrayList<String>();
list.add("1");
list.add("2");
Stream stream = list.stream();
list.add("3");
System.out.println(stream.count()); // 3
```
#### Chaining Optionals 

```java
private static void threeDigit(Optional<Integer> optional) {
    if(optional.ifPresent()) {
        Integer num = optional.get();
        String string = "" + num;
        if( string.length() == 3 ) {
            System.out.println(string)
        }
    }
}
private static void threeDigit(Optional<Integer> optional) {
    optional
        .map(n -> "" + n) 
        .filter(s -> s.length() == 3)
        .ifPresent(System.out::println)
}
```

```java
public static void main(String[] args) {
    Optional<String> optional = Optional.of("A");
    Optional<Integer> s = optional.map(String::length);
    Optional<Integer> s2 = optional.map(Main::calculator);
    Optional<Integer> s3 = optional.flatMap(Main::calculator);
}
static Optional<Integer> calculator(String s) {
    return Optional.of(1);
}
```

#### Collecting Results
Examples of grouping/partitioning

| Collector | Decription | Return value when passed to collect |
|-----------|------------|-------------------------------------|
| averagingDouble(ToDoubleFunction f)<br>averagingInt(ToIntFunction f)<br>averagingLong(ToLongFunction f) | Calculates the average for our three core primitive types | Double |
| counting() | Counts number of elements | Long |
| groupingBy(Function f)<br>groupingBy(Function f, Collector dc)<br>groupingBy(Function f, Supplier s, Collector cd) | Creates a map grouping by the specified function with the optional type and optional downstream collector | Map<K, List<T>> |
| joining()<br>joining(CharSequence cs) | Create a single String using cs as a delimiter between elements if one is specified | String |
| maxBy(Comparator c)<br>minBy(Comparator C) | Funds the largest/smallest elements | Optional<T> |
| mapping(Function f, Collector dc ) | Adds another level of collectors | Collector |
| partitioninBy(Predicate p)<br>partitioningBy(Predicate p, Collector dc) | Creates a map grouping by the specified predicate with the optional further downstream collector | Map<Boolean, List<T>> |
| summarizingDouble(ToDoubleFunction)<br>summarizingInt(ToIntFunction)<br>summarizingLong(ToLongFunction) | Calculates average, min, max, and so on | DoubleSummaryStatistics<br> IntSummaryStatistics<br>LongSummaryStatistics |
| summingDouble(ToDoubleFunction f)<br>summingInt(ToIntFunction f)<br>summingLong(ToLongFunction f) | Calculates the sum for our three core primitive types | Double<br>Integer<br>Long |
| toList()<br>toSet() | Creates an arbitrary type of list or set | List <br> Set |
| toCollection(Supplier s) | Creates a collection of the specified type | Collection |
| toMap(Function k, function v)<br> toMap(Function k, Function v, BinaryOperator m)<br> toMap(Function k, Function v, BinaryOperator m, Supplier s) | Creates a map using the keys, values, an optional merge function, and optional type | Map |

#### Collection Using Basic Collector 

At this point, you shoeld be able to use all of the Collectors, except `groupingBy()`, `mapping()`, `partitioningBy()`, and `toMap()`

```java
Stream<String> ohMy = Stream.of("lions", "tigers", "bears");
String result = ohMy.collect(Collectors.joining(", "));
System.out.println(result); // lions, tigers, bears

Double result2 = ohMy.collect(Collectors.averagingInt(String::length));
System.out.println(result2); // 5.3333333333

TreeSet<String> result3 = ohMy.filter(s -> s.startsWith("t")).collect(Collectors.toCollection(TreeSet::new));
System.out.println(result3); // [Tiger]
```

#### Collection into Maps

```java
Stream<String> ohMy = Stream.of("lions", "tigers", "bears");
TreeMap<Integer, String> treeSet = ohMy.collect(Collectors.toMap(k -> k.length(), v -> v, (s1, s2) -> s1 + "," + s2, TreeMap::new));
// {5=,lions,bears, 6=tigers}
System.out.println(map.getClass()); // class .java.util.TreeMap
```

#### Collection Using Grouping, Partitioning, and Mapping

```java
Stream<String> ohMy = Stream.of("lions", "tigers", "bears");
Map<Integerm List<String>> map = ohMy.collect(Collectors.groupingBy(String::length));
System.out.println(map); // {5=[lions, bears], 6=[tigers]}

Map<Integerm Set<String>> map = ohMy.collect(Collectors.groupingBy(String::length, Collectors.toSet()));
TreeMap<Integerm Set<String>> map = ohMy.collect(Collectors.groupingBy(String::length, TreeMap::new, Collectors.toSet()));
TreeMap<Integerm List<String>> map = ohMy.collect(Collectors.groupingBy(String::length, TreeMap::new, Collectors.toList()));

Map<Integer, Long> map = ohMy.collect(Colectors.groupingBy(String::length, Collectors.counting()));
```

**Partitioning**
- Is like splitting a list into two parts.
- Is a special case of grouping. With partitioning, there are only two possible groups `true` and `false`
- Unline `groupingBy()` we cannot change the type of Map that gets returned.
- Instead of using the downstream collector to specify the type, we can use the collectos that we've already shown

```java
Stream<String> ohMy = Stream.of("lions", "tigers", "bears");
Map<Boolean, List<String>> map = ohMy.collect(Colectors.partitioningBy(s -> s.length() <= 5));
// {false=[tigers], true=[lions, bears]}
Map<Boolean, List<String>> map = ohMy.collect(Colectors.partitioningBy(s -> s.length() <= 7));
// {false=[], true=[lions, bears, tigers]}
Map<Boolean, Set<String>> map = ohMy.collect(Colectors.partitioningBy(s -> s.length() <= 7, Collectors.toSet()));
Map<Boolean, Set<String>> map = ohMy.collect(Colectors.partitioningBy(s -> s.length() <= 7, Collectors.counting()));
// {false=[], true=[lions, bears, tigers]}
```

**Mapping()**
- That lets us go down a level and add another collector.
- Always takes two parameters: the function for the value and how to group it further
- You might see collectos used with a static import to make the code shorter
```java
// We wanted to get the first letter of the forst animal alphabetically of each length
Stream<String> stream = Stream.of("lions", "tigers", "bears");
Map<Integer, Optional<Character>> map = stream.collect(Collectors.groupingBy(String::length,
        Collectors.mapping(s -> s.charAt(0), Collectors.minBy(Comparator.naturalOrder()))));
```

#### Debugging Complicated Generics
Here are three useful techniques for dealing with this situation:
- Start over with a simple statement and keep adding to it. By making one tiny change at a time, you will know which code introduced the error.
- Extract parts of the statement into separate statements. 
- Use generic wildcards for the return type of the final statement

