### Diferença entre um Objeto e uma referência
- O tipo do objeto determina quais propriedades existem dentro do objeto em memória
- O tipo da referência determina quias métodos e variaveis estão acessiveis no código Java

### Cast Object References
- Se fomos fazer cast de um objeto para sua super classe não precisa de cast explícito;
- Se for ao contrário é necessário o cast;
- O compilador não deixar fazer cast de tipos não relacionados;
- Sempre que fomos realizar um cast explícito é possíveis que seja lançado uma runtime exeception, se o objeto antes do cast não uma instancia da classe;

###  Java Beans
- As propriedades da classe devem ser private
- Getters para prorpiedades não boolean devem começar com get
- Getters para propriedades booleans devem começar com is ou get
- Setter começam com set

A segunite linha é incorreta para ser um javabean, já que somente o método get só pode começar com is se a propriedade for um boolean, neste caso é um wrapper de boolean, ou seja um objeto.
```java
public Boolean isDancing() {
    return dancing;
}
```


