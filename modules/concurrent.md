### Concorrência e Paralelismo
 - Mecanismos para trabalhar com programação concorrente
    - Classe Thread
    - Interface Runnable
    - Modificador Synchronized
    - Métodos wait(), notify() e notifyAll()
- Além do pacote java.concurrent existem dois novos pacotes java.concurrent.locks e java.concurrent.atomic


### Runnable 
- Esta interface é implementada por classes que desejam ter seu código executado numa thread separada.
- Esta classe deve implementar o método `run()`

### O método run()
- Não pode retornar valor
- Não pode lançar Checked Exceptions

### Callable
- Criada para resolver os problemas de Runnable.
- Ela possui um genérics que irá indicar qual o tipo de dado i método `call()` irá chamar 
- Possui um método chamado `call()`
- Consigo lançar Checked Exceptions

### Criação de Threads
- Não é necessário ficar chamando a Classe Thread toda hora
- A execução das Thread fica a cargo dos executores (`executors`). Criam e executam Threads.
```java
ExecutorService e = Executors.newSingleThreadExecutor(); // Criando uma thread
e.execute(() -> System.out.println("")); // Executando a Thread
e.shutdown(); // Matando a thread

Executors.newFixedThreadPool(5); // Criando um Pool de Thread
```

### Sincronização 
- Existem porções de código que não podem ser executadas por duas threads ao mesmo tempo. Isso a gente chama de Região Crítica.
- O Java trabalha com o conceito de Monitor, que garante a sincronização entre as threads.
    - Um monitor pode ser qualquer Objeto
    - Cada monitor possui um lock, que é entregado à thread que acessa a região crítica;
    - Threads sem lock, aguardam

### java.util.concurrent.lock
- Não é mais necessário utilizar o modificador synchronized 
- Um novo conceito de lock é utilizado.
    - `ReentrantLock` e `ReentrantReadWriteLock`

### ReentrantLock
- Quandoa thread chama o método `lock()` no objeto, ele possui acesso exclusivo, até que chame o método unlock().
- Se outra thread chamar o método `lock()`, ficará aguardando numa fila
```java
ReentrantLock r = new ReentrantLock();
r.lock();
try {
    
}finally {
    r.unlock();
}
```

### ReentrantReadWriteLock
- Possui um par de locks, um para leitura e outro para escrita.
- O lock de leitura pode ser obtido por várias threads simultaneamente.
- O lock de escrita é exclusivo, se alguma thread tiver o lock de escrita, nenhuma outra thread pode estar lendo ou escrevendo
```java
ReentrantReadWriteLock rw = new ReentrantReadWriteLock();
Lock read = rw.readLock();
Lock write = rw.writeLock();
```

### Comunicação entre as threads.
```java
ReentrantLock r = new ReentrantLock();
Condition c = r.newCondition();
r.lock();
try {
    while(count > 10) {
        c.await();
    }
    c.signal();
}finally {
    r.unlock();
}		
```

| Forma Clássica | Usando Condições |
| ------ | ------ |
| wait() |  await() |
| notify() | signal() | 
| notifyAll() | signalAll() | 

### Tarefas Futuras 
- A classe FutureTask possibilita que um thread retorne um dado assíncrono futuro.
```java
ExecutorService e = Executors.newSingleThreadExecutor();
e.execute(() -> System.out.println(""));
FutureTask<Integer> ft = new FutureTask<Integer>(h);
e.execute(ft);
Integer resultado = ft.get();
e.shutdown();
```


### Operações atômicas
- O Java 8 veio com algumas classes para trabalhar de forma atomica sem necessidade de utilizar o lock
```java
import java.util.concurrent.atomic.*;
AtomicInteger ai = new AtomicInteger(10);
return ai.incrementAndGet();
```

### Fork and Join
- Executar varias tarefas ao mesmo tempo com um objetivo comum
- Abstrai a complexidade da programação paralela
- Divisão de problemas, em problemas menores
- É baseada em duas operações:
    - *Fork*: Cria uma subtarefa para ser executada de forma assíncrona
    - *Join*: Aguarda até a subtarefa ser completada
- Uma tarefa deve ser uma classe que herde de RecursiveTask ou RecursiveAction
    - *RecursiveTask*: São tarefas que retornam informação
    - *RecursiveAction*: Tarefas que não retornam informação
```java
import java.util.concurrent.RecursiveTask;
public class SumArray extends RecursiveTask<Long> {
	@Override
	protected Long compute() {
		SumArray sumArray = new SumArray();
		sumArray.fork();
		// Faz o processamento necessário
		return sumArray.join();
	}
}
```

### Iniciando a execução de Tarefas 
- O ínicio é feito através do método `invoke()` da classe ForkJoinPool
```java
SumArray task = new SumArray();
ForkJoinPool fj = new ForkJoinPool();
Long result = fj.invoke(task);
```