# Java Class Design and Advance Java Class Desing
- [x] Implement inherintance incluinding visibility modifiers and composition
- [X] Implment polymorphism
- [X] Override hashCode, equal, and toString methods from Object class
- [X] Develop code that uses the static keyword on initialize blocks, variables, methods, and classes

#### Access Modifiers

| Can access | If member is private? | If If that member has default (package private access) | If that member isprotected? | If that member is public? |
|:-------------| :-------------| :-------------| :-------------| :-------------|
| Member in the same class | yes | yes | yes | yes |
| Member in another class in the same package | no | yes | yes | yes |
| Member in a superclass in a different package| no| no| yes| yes |
| Method/ field in a class (that is not a superclass) in a different package | no | no | no | yes |


#### When multiple overloaded methods are present, Java looks for the closest match firts. It tries to find the following:
- Exact match by type
- Matching a superclass type
- Converting to a larger primitive type
- Converting to an autoboxed type
- Varags

#### For overriding, the overridden method has a few rules:
- The access modifier must be the same or more accessible
- The return type must be the same or a more restrictive type, also known as `covariant return types`
- If any checked execptions are thrown, only the same exceptions or subclasses of those exceptions are allowed to be thrown.
- The methods must not be static.

#### Abstract class
- Abstract class is not requered to have any methods in it, let alone any abstract ones.
- `abstract void clean()`
- Default implementations

#### Static and Final
- `static` cannot be added in a method that the subclass overrides it.
- `final` keyword cannot be added to a abstract method or abstract class becouse the method would no longer be able to override it.
- `final` prevents a variable from changing or a method from being overridden. 
- `static` makes a variable shared at the class level and uses the class name to refer to a method.

#### Imports 
- You can import the static way some resources like `import static java.util.Collections.sort` or `import static java.util.Collection.*`
- The `java.lang.*` alwas is imported.

#### Using instanceof
- In `a instanceof B`, the expression returns true if the reference to which a points is an instance of class B, a subclass of B ( directly or indirectly), or a class that implements the B interface (directly or indirectly).
- The compiler knos an interface could be added, so the `instanceof` statement could be true for some subclasses, whereas there is no possible way to turn a hippo into Elephant.
- The `instanceof` operator is commonly used to determine if an instance is a subclass of a particular object before applying an explicit cast.

#### Virtual Method Invocation
- A Virtual Method is a regular non-static methods. Java looks for an overridden method rather than necessarily using the one in the class that the compiler says we have. 
- In this case the class `Lion` doesn't have the overridden method `printName`
- Java looked at the actual `type` of `animal` at runtime and called feed on that.
```java
abstract class Animal {
    String name = "????";
    public void printName() {
        System.out.println(this.name);
    }
}

class Lion extends Animal {
    String name = "Leão de la selva";
}

class Playground {
    public static void main(String[ ] args) {
       Animal animal = new Lion();
       animal.printName();
    }
}
```

#### Annottiong Overridden methods
When you see `@Override` show up on the exam, you must check carefully that the method is doing one of three things:
- Implementing a method from an interface
- Overriding a superclass ethod of a class shown in the example
- Overrinding a method declared in Object, such as `hasCode`, `equals` or `toString`.

#### Coding equals, hashCode and toString
##### toString:
- It doesn't haves parameters and It must returns a `String` type.

##### equals:
- Java uses `==` to compare primitives and for checking if two variables refer to the same object.
- Checking if two objects are equivalent uses the `equals()` method
- `String` does have an `equal()` method, It checks that the values are the same.
- `StringBuilder` uses the implementation of `equals()`, which simply checks if the two objects being referred to are the same.

The `equals()` method implements an equivalence relation on non-null object references:
- It is reflexive: For any non-null reference value x, `x.equals(x)` should return true.
- It is symmmetric: For any non-null reference values x, `x.equals(y)` should return true if and only if `y.equals(x)` returns true;
- It is transitive: For any non-null reference values, x, y and z, if `x.equals(y)` return true and `y.equals(z)` returns true, then `x.equals(z)` should return true.
- It is consistent: For any non-null reference values x and y, multuple invocations of `x.equals(y)` consistently return true or consistently return false, provided no information used in equals comparisons on the object is modified.
- For any non-null reference value x, `x.equals(null)` should return false.

#### hashCode
- The `hashCode` is used when storing the object as a key in a map.
- A hash code is a number that puts intances of a class into a finite number of categories.
- Whithin the same program, the result of `hashCode()` must not change.
- If `equals()` return true when called with two objects, calling `hashCode()` on each of those objects must return the same result.
- If `equals()` return false when called with two objects, calling `hashCode()` on each of those objects does not have to return the same result.

### working with Enums
- In Java, an `enum` is a class that represents an enumeration. It is much better than a bunch of constants because it provides type-safe checking.
To create an enum, use the `enum` word:
```java
public enum Season {
    SUMMER, WINTER, FALL, SPRING;
}
```
The enum includes some helper methods like `name()`. Also It print the name of the enum when `toString()` is called. They are also comparable using `==` because they are like `static final` constants;
```java
Season s = Season.SUMMER;
Sysout.out.println(Season.SUMMER); //SUMMER
Sysout.out.println(s == Season.SUMMER); //true
```
An enum provides a method to get an array of all of the values. You can use this like any normal array, including in a loop:
```java
for(Season s : Season.values()) {
    Sysout.out.println(s.name() + " " + s.ordinal()); //true    
}
```
- You can't compare an int and enum because enum is a type
- You can't extend an enum.
- You can create an enum from a String.
```java
Season s = Season.valueOf("FALL"); // FALL
Season d = Season.valueOf("fall"); // IllegalArumentException
```

#### Adding Constructors, Fields, and Methods
- The semicolon at the end of the enum values is optional only if the only thing in the enum is that list of values
- The constructor is private because it can only be called from within the enum. The code will not compile with a public constructor.
```java
public enum Season {
    WINTER("Low"), SPRING("Medium"), SUMMER("High"), FALL("Medium");
    private String expectedVisitors;
    private Season(String expectedVisitors) {
        this.expectedVisitors = expectedVisitors;
    }
    public void printExpectedVisitors() {
        System.out.println(expectedVisitors);
    }
}
public class Main {
    public static void main(String[] args) {
        Season.SUMMER.printExpectedVisitors();
    }
}
```
- The first time that we ask for any of the enum values, Java constructs all of the enum values.
- The enum bellow itself has an abstract method. This means that each and every enum value is required to implement this method. If we forget one, we get a compile error.
- If we don't want each and every enum value to have a method, we can create a default implementation and override it only for the special cases.
```java
public enum Season2 {
    SUMMER {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    }, WINTER {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    },
    SPRING {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    },
    FALL {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    };

    public abstract void printHours();
}
public enum Season3 {
    SUMMER {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    }, WINTER {
        public void printHours() {
            System.out.println("9am-7pm");
        }
    },
    SPRING, FALL;

    public void printHours() {
        System.out.println("default hours");
    }
}
```

### Creating Nested Classes
- A nested class is a class that is defined within annother class
- A nested class that is not static is called an `inner class`.

There are four of types of nested classes:
1. A member inner class is a class defined at the same level as instance variable. It is not static. Often, this is just referred to as an inner class without explicitly saying the type.
1. A local inner class is defined within a method.
1. An anonymous inner class is a special case of a local inner class that does not have a name
1. A static nested class is a static class that it defined at the same level as static variables.

Benefits of using inner classes:
- They can encapsulate helper classes by restricting them to the containing class.
- They can make it easy to create a class that will be used in only one place.
- They can make the code easier to read.
- They can also make the code harder to read when used improperly

#### Member Inner Classes 
- The member inner class is defined at the member level (the same level as the methods, instances variables, and constructors).
- For the Inner Class, the compiler creates `Outer$Inner.class`
- The Inner Class, can have the same variable names as outer classes.
- You also aren't limited to just one inner class.
- Inner Class can implements a secret interface that is declarates inside de OuterClass
- Member inner class have the following properties:
    - Can be declared public, private or protected or use default access
    - Can extend any class and implement interfaces
    - Can be abstract or final
    - Cannot declare static fields or methods
    - Can access members of the outer class including private members

```java
public class OuterClass {
    private String greeting = "HI";
    public class InnerClass {
        public void printHi() {
            System.out.println(greeting);
        }
    }
    public void sayHi3Times() {
        InnerClass in = new InnerClass();
        for(int i = 0; i <= 3; i++) {
            in.printHi();
        }
    }
    public static void main(String[] args) {
        OuterClass out = new OuterClass();
        Inner inner = out.new Inner();
        inner.go();
        out.sayHi3Times();
    }
}
```

```java
public class A  {
    private int x = 10.
    class B {
        private int x = 20;
        class C {
            private int x = 30;
            public void alltheX() {
                System.out.println(this.x);
                System.out.println(B.this.x);
                System.out.println(A.this.x);
            }
        }
    }
    public static void main(String[] args) {
        A a = new A();
        A.B b = a.new B();
        A.B.C c = b.new C();
        c.alltheX();
    }
    
}
```

#### Local Inner CLasses
- A local inner class is a nested classes defined within a method, like a local variables.
- A local inner class does not exist until the method is involed
- You can create instances only from within methods.
- They do not have an access specifier
- They cannot be declared static and cannot declare static fields or methods
- They have access to all fields and methods of the enclosing class
- They do not have access to local variables of a method unless those variables are final or effectively final

``` java
public class Outer {
    private int length = 5;
    public void calculate() {
        final int width = 20;
        class Inner {
            public void multiply() {
                System.out.println(width * length);
            }
        }
        Inner n = new Inner();
        n.multiply();
    }
}
```

#### Anonymous Inner Classes
- An anonymous inner class is a local inner class that does not have a name
- Anonymous inner classes are required to extend an existing class or implment an existing interface

```java
public class AnonInner {
    abstract class SaleTodayOnly {
        abstract int dollarsOff(); 
    }
    public int admission(int basePrice) {
        SaleTodayOnly s = new SaleTodayOnly() {
            int dollarsOff() {
                return 3;
            }
        };
        return basePrice - s.dollarsOff();
    }
}
```

#### Static Nested Classes
- The final tye of nested class is not an inner class, a static nested class is a static class defined at the member leve.
- I can't access the instance variables without an explicit object of the enlosing class.
- The nesting creates a namespace because the enclosing class name must be used to refer to it.
- It can be made private or use one of the other access modifiers to encapsulate it.
- The enclosing class can refer to the fields and methods of the static nested class.
- You can import it using a regular import `import br.com.sicoob.Outer.Inner` or using a static import `import static br.com.sicoob.Outer.Inner`


| | Member Inner Class | Local Inner Class | Anonymous inner class | Static nested class |
|:-------------| :-------------| :-------------| :-------------| :-------------|
| Access Modifiers allowed | - public<br>- protected<br>- private<br>- default access | none. Already local to method | none. Already local to statement | -public<br>- protected<br>- private<br>- or default access |
| Can extend any class and any number of interfaces | Yes | Yes | No-must have exaclty one superclass or one interface | Yes |
| Can be abstract | Yes| Yes | N/A because no class definition | Yes |
| Can be final | Yes | Yes | N/A because no class definition | Yes |
| Can access instance members of enclosing class | Yes | Yes | Yes | No (not directly, requires and instance of the enclosing class) |
| Can access local variables of enclosing class | No | Yes (if final or effectively final) | Yes (if final or effectively final) | No |
| Can declare static members | No | No | No | Yes |
