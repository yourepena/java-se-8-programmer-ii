package ocp;

abstract class AnimalX {
	
	public String name;

	void printName() {
		System.out.println(this.name);
	}

	void careFor() {
		this.play(this.name);
	}

	void play(String name) {
		System.out.println("Eu sou um animal: " + name);
	}
}

class Lion extends AnimalX {
	public String name = "Leo";

	void play(String name) {
		System.out.println("Eu sou um leao: " + name);
	}
	
	void method() {
		
	}
}

public class Animal {
	public static final String OH_LOQUINHO_MEU = "asdasd";

	public static void main(String[] args) {
		StringBuilder s1 = new StringBuilder("1");
		StringBuilder s2 = new StringBuilder("1");
		System.out.println(s2.toString() == s1.toString());
		System.out.println(s2.toString().equals(s1.toString()));
		System.out.println(Season.FALL.ordinal());
	}
}

enum Season {
    SUMMER("high") {
    	public void print() {
        	System.out.println("Mais horas");
        }	
    }, WINTER("low"), SPRING("medium"), FALL("medium");// Semicolon no final da linha
	public String publico;
    private Season(String publico) { // Constructor privado.
        this.publico = publico;
    } 
    public void print() {
    	System.out.println("Default horas");
    }

}