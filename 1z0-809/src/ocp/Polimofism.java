package ocp;

public class Polimofism {
	
	static interface Browser {
		 
	}
	
	static class Firefox implements Browser {
		public void go() {
			System.out.println("BLabalba");
		}
	}
	
	static class IE extends Firefox {
		public void go() {
			System.out.println("Chahcahca");
		}
	}
	
	public static void main(String[] args) {
		IE b = new IE();
		Browser i = b;
		Firefox d = b;
		int result = 0;
		if( b instanceof Browser) result += 1;
		if( b instanceof Firefox) result += 2;
		System.out.println(result);
	}

}
