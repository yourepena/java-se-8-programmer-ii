package ocp;

import static ocp.Animal.*;

public class OuterClass {
	/// erros na linha 3, 5, 8
	
	enum Vla {
		A, B, C
	}
	
	private void sysout() {
		 System.out.println(OH_LOQUINHO_MEU);

	}
	
	private String greeting = "Hi";
	public class InnerClass {
		
		public int repeat = 3;
		private static final String greeting = "Hi";

		public void go() {
			for (int i = 0; i < repeat; i++) {
				System.out.println(InnerClass.greeting);
			}
		}
	}
	
	private String a;
	
	public void test() {
		int b = 0;
		class InnerC {
			public int c= b;
		}
	}
	
	public void callInnerClass() {
		InnerClass ic = new InnerClass();
		ic.go();
	}
	
	public static void main(String[] args) {
		OuterClass.InnerClass oi = new OuterClass().new InnerClass();
		
		System.out.println(Vla.A.ordinal());
		
		String s1 = "Canda";
		String s2 = new String(s1);
		if(s1 == s2) System.out.println("a");
		if(s1.equals(s2)) System.out.println("b");
		if(s1.hashCode() == s2.hashCode()) System.out.println("c");
	}

}
