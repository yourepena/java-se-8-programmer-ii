package assessment;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;

class Tail<T> {
	
	T value;
	
	public Tail(T value) {
		this.value = value;
	}
	
	public T getValue() {
		return value;
	}
	
}

public class Bird implements Serializable {
	
	
	
		
	private String name;
	private transient int age;
	private Bird tail;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Bird getTail() {
		return tail;
	}
	public void setTail(Bird tail) {
		this.tail = tail;
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		Tail<String> one = new Tail<String>("you ");
		Tail<Integer> two = new Tail<Integer>(123);
		
		System.out.println(one.getValue());
		System.out.println(two.getValue());
	}
	
}