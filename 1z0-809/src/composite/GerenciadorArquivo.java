package composite;

public class GerenciadorArquivo {

	public static void main(String[] args) {


		Pasta pasta = new Pasta();
		
		pasta.add(new PDFArquivo());
		
		Pasta pasta2 = new Pasta();
		pasta.add(pasta2);
		pasta2.add(new PNGArquivo());
		pasta2.add(new PNGArquivo());
		pasta2.add(new PNGArquivo());
		pasta2.add(new PNGArquivo());
		
		
		pasta.download();
		
	}

}
