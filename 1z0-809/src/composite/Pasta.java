package composite;

import java.util.ArrayList;
import java.util.List;

public class Pasta extends Arquivo {
	
	private List<Arquivo> children = new ArrayList<>();
	
	public void add(Arquivo el) {
		this.children.add(el);
	}
	
	public void remove(Arquivo el) {
		this.children.remove(el);
	}

	@Override
	public void download() {
		 for (Arquivo elemento : children) {
			elemento.download();
		}

	}

}
