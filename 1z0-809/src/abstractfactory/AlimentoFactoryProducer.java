package abstractfactory;

public class AlimentoFactoryProducer {

	public static AbstractFactoryAlimento getFactory(String tipo) {
		if (tipo.equals("organico")) {
			return AlimentoOrganicoFactory.getInstance();
		} else if (tipo.equals("industrial")) {
			return AlimentoIndustrialFactory.getInstance();
		}
		return null;
	}

}
