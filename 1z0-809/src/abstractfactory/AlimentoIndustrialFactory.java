package abstractfactory;

public class AlimentoIndustrialFactory extends AbstractFactoryAlimento {
	
	private  static AlimentoIndustrialFactory instance;
	
	
	private AlimentoIndustrialFactory() {}
	
	public static AlimentoIndustrialFactory getInstance() {
		if(instance == null) {
			return new AlimentoIndustrialFactory();
		}
		return instance;
	}

	public Alimento newAlimento(TiposAlimento tipo) {
		if (tipo.equals(TiposAlimento.PEPINO)) {
			return new PepinoIndustrial();
		} else if (tipo.equals(TiposAlimento.TOMATE)) {
			return new TomateIndustrial();
		}
		return null;
	}

}
