package abstractfactory;

public class AlimentoOrganicoFactory extends AbstractFactoryAlimento {
	
	private static AlimentoOrganicoFactory instance;
	
	private AlimentoOrganicoFactory() {}
	
	public static AlimentoOrganicoFactory getInstance() {
		if(instance == null) {
			return new AlimentoOrganicoFactory();
		}
		return instance;
	}


	public Alimento newAlimento(TiposAlimento tipo) {
		if (tipo.equals(TiposAlimento.PEPINO)) {
			return new PepinoOrganico();
		} else if (tipo.equals(TiposAlimento.TOMATE)) {
			return new TomateOrganico();
		}
		return null;
	}

}
