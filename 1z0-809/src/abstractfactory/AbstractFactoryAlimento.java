package abstractfactory;

public abstract class AbstractFactoryAlimento {

	public abstract Alimento newAlimento(TiposAlimento tipo);
}
