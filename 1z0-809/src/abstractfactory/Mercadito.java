package abstractfactory;

public class Mercadito {

	public static void main(String[] args) {
		AbstractFactoryAlimento factory = AlimentoFactoryProducer.getFactory("industrial");
		Alimento alimento = factory.newAlimento(TiposAlimento.TOMATE);
		System.out.println(alimento.nome);
	}

}
