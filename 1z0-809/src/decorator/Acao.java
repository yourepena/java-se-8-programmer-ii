package decorator;

public abstract class Acao {
	
	private double preco;
	
	public abstract void comprar(Preco precoAtual);

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

}
