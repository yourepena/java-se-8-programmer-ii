package decorator;

public class DescerDecorator extends MercadoDecorator {

	private double percento;
	
	public DescerDecorator(Acao a, int percento) {
		super(a);
		this.percento = percento;
	}

	@Override
	public void comprar(Preco precoAtual) {
		double parte =  ( precoAtual.getPrecoAtual()* this.percento)/100;
		precoAtual.setPrecoAtual(precoAtual.getPrecoAtual() - parte);
		super.comprar(precoAtual);
	}

}
