package decorator;

public class HomeBroker {
	
	public static void main(String[] args) {
		BPAN bpan = new BPAN();
		Preco p = new Preco();
		p.setPrecoAtual(20);
		bpan.setPreco(p.getPrecoAtual());
		Acao sb = new SubirDecorator(new DescerDecorator(new SubirDecorator(bpan, 1), 10), 2);
		sb.comprar(p);
	}

}
