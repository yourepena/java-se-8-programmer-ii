package decorator;


public class SubirDecorator extends MercadoDecorator {
	
	private double percento;
	
	public SubirDecorator(Acao a, int percento) {
		super(a);
		this.percento = percento;
	}

	@Override
	public void comprar(Preco precoAtual) {
		double parte =  ( precoAtual.getPrecoAtual()* this.percento)/100;
		precoAtual.setPrecoAtual(precoAtual.getPrecoAtual() + parte);
		super.comprar(precoAtual);
	}

	 

}
