package decorator;

public abstract class MercadoDecorator extends Acao {
	
	public Acao acao;
	
	public MercadoDecorator(Acao acao) {
		this.acao = acao;
	}

	@Override
	public void comprar(Preco precoAtual) {
		acao.comprar(precoAtual);
	}

}
