package decorator;

public class BPAN extends Acao {

	@Override
	public void comprar(Preco precoAtual) {
		if(this.getPreco() > precoAtual.getPrecoAtual()) {
			System.out.println("Comprando banco PAN papai a " + precoAtual.getPrecoAtual());
		} else {
			System.out.println("Vendendo banco PAN papai a " + precoAtual.getPrecoAtual());
		}
	}

}
