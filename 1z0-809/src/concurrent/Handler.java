package concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Handler implements Callable<Integer>{
	private int x = 10;
	private int y = 20;
	
	Handler(int x, int y){
		this.x = x;
		this.y = y;
	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		int count = 0;
		Handler h = new Handler(10, 20);
		
		ExecutorService e = Executors.newSingleThreadExecutor();
		e.execute(() -> System.out.println(""));
		FutureTask<Integer> ft = new FutureTask<Integer>(h);
		e.execute(ft);
		Integer resultado = ft.get();
		e.shutdown();

		
	  /*e = Executors.newFixedThreadPool(5);
		e.execute(() -> System.out.println(""));
		e.shutdown(); */		
		
		
		ReentrantLock r = new ReentrantLock();
		Condition c = r.newCondition();
		r.lock();
		try {
			while(count > 10) {
				c.await();
			}
			c.signal();
		}finally {
			r.unlock();
		}
		
		ReentrantReadWriteLock rw = new ReentrantReadWriteLock();
		Lock read = rw.readLock();
		Lock write = rw.writeLock();

	}

	@Override
	public Integer call() {
		AtomicInteger ai = new AtomicInteger(10);
		return ai.incrementAndGet();
	}

}
