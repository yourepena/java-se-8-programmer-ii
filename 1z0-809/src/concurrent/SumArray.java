package concurrent;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
public class SumArray extends RecursiveTask<Long> {
	@Override
	protected Long compute() {
		SumArray sumArray = new SumArray();
		sumArray.fork();
		// Faz o processamento necessário
		return sumArray.join();
	}
	
	public static void main(String[] args) {
		SumArray task = new SumArray();
		ForkJoinPool fj = new ForkJoinPool();
		Long result = fj.invoke(task);

	}
}
