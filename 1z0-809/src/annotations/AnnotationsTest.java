package annotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AnnotationsTest {

	int x = 10;

	@Synchronized(false)
	public void opa() {
	}

	public static void main(String[] args) throws ClassNotFoundException {
		List<Double> list1 = Arrays.asList(10.5, 3.2, 4.6);
		List<Long> list2 = list1.stream()
			.map(n ->  Math.round(n))
			.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

		
		List<Alimento> alimentos = Arrays.asList(new Alimento("Abacaxi", 5.4), new Alimento("Maçã", 12.2), new Alimento("Abacate", 9D));
		
		List<Double> i = alimentos.stream().map(e -> e.preco).collect(Collectors.toList());
		i.forEach(System.out::println);
 
	}

}

class Alimento {

	public String nome;
	public Double preco;
	
	public Alimento(String nome, Double preco) {
		this.nome = nome;
		this.preco = preco;
	}
	
	 
}
