package adapter;

public class Tomada {

	private int entradas;

	public Tomada() {
		this.entradas = 2;
	}

	public int getEntradas() {
		return entradas;
	}

	public void setEntradas(int entradas) {
		this.entradas = entradas;
	}

}
