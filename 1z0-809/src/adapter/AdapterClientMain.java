package adapter;

public class AdapterClientMain {

	public static void main(String[] args) {
		TomadaAdapterAmericana tm = new TomadaAdapterAmericana();
		tm.setEntradas(2);
		System.out.println(tm.getEntradas());
		System.out.println(tm.getEntradasSuper());
	}

}
