package adapter;

public class TomadaAdapterAmericana extends Tomada {
	
	
	public int getEntradas() {
		return super.getEntradas() - 1;
	}
	
	public int getEntradasSuper() {
		return super.getEntradas();
	}

	public void setEntradas(int entradas) {
		super.setEntradas(entradas + 1);
	}


}
