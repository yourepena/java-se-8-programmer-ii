package observer;

public interface Subject {
	
	void registerObservable(Observer obj);
	void removeObservable(Observer obj);
	void notificateAll();

}
