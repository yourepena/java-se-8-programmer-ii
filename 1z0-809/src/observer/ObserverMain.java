package observer;

public class ObserverMain {

	public static void main(String[] args) {
		Observer o = new ConsoleObserver();
		MusicaSubject ms = MusicaSubject.getInstance();
		ms.registerObservable(o);
		ms.notificateAll();
	}

}
