package observer;

import java.util.LinkedHashSet;
import java.util.Set;

public class MusicaSubject implements Subject {
	
	private static MusicaSubject instance;
	
	Set<Observer> observers = new LinkedHashSet<Observer>();

	private MusicaSubject() {
	}
	
	public static MusicaSubject getInstance() {
		if(instance == null) {
			instance = new MusicaSubject();
		}
		return instance;
	}
	
	@Override
	public void registerObservable(Observer obj) {
		observers.add(obj);
	}

	@Override
	public void removeObservable(Observer obj) {
		observers.remove(obj);
	}

	@Override
	public void notificateAll() {
		for (Observer observer : observers) {
			observer.update(this);
		}

	}

}
